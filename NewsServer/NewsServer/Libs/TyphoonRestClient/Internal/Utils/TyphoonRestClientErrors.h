////////////////////////////////////////////////////////////////////////////////
//
//  AppsQuick.ly
//  Copyright 2015 AppsQuick.ly
//  All Rights Reserved.
//
//  NOTICE: This software is the proprietary information of AppsQuick.ly
//  Use is subject to license terms.
//
////////////////////////////////////////////////////////////////////////////////

// Error Domain
static NSString *TyphoonRestClientErrors = @"TyphoonRestClientErrors";

// Error Codes
static NSInteger TyphoonRestClientErrorCodeValidation = 100;
static NSInteger TyphoonRestClientErrorCodeTransformation = 101;
static NSInteger TyphoonRestClientErrorCodeRequestUrlComposing = 103;
static NSInteger TyphoonRestClientErrorCodeRequestSerialization = 102;
static NSInteger TyphoonRestClientErrorCodeResponseSerialization = 104;
static NSInteger TyphoonRestClientErrorCodeBadResponseCode = 105;
static NSInteger TyphoonRestClientErrorCodeBadResponseMime = 106;
static NSInteger TyphoonRestClientErrorCodeConnectionError = 107;

// UserInfo Dictionary Keys
static NSString *TyphoonRestClientErrorKeyFullDescription = @"TyphoonRestClientErrorKeyFullDescription";
static NSString *TyphoonRestClientErrorKeySchemaName = @"TyphoonRestClientErrorKeySchemaName";

static NSString *TyphoonRestClientErrorKeyResponseData = @"TyphoonRestClientErrorKeyResponseData";
static NSString *TyphoonRestClientErrorKeyResponse = @"TyphoonRestClientErrorKeyResponse";

static NSString *TyphoonRestClientErrorKeyOriginalError = @"TyphoonRestClientErrorKeyOriginalError";