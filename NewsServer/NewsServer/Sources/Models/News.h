//
//  News.h
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    NewsPriorityLow,
    NewsPriorityNormal,
    NewsPriorityHight
}NewsPriority;

typedef enum{
    NewsStatusAccepted,
    NewsStatusRejected,
    NewsStatusModeration
}NewsStatus;

@interface News : NSObject

@property (nonatomic, strong) NSString* newsfeedName;
@property (nonatomic, strong) NSString* subject;
@property (nonatomic, strong) NSString* body;
@property (nonatomic) NewsPriority priority;
@property (nonatomic) BOOL isPrivate;
@property (nonatomic, strong) NSArray* tags;
@property (nonatomic) NSNumber *ttl;
@property (nonatomic) NSString *newsID;
@property (nonatomic) NSDate *createdDate;
@property (nonatomic) NSDate *deathDate;
@property (nonatomic) NSUInteger votesPro;
@property (nonatomic) NSUInteger votesContra;
@property (nonatomic) NewsStatus newsStatus;
@property (nonatomic) NSString *authorName;

- (NSString *)priorityString;
- (NSString *)tagsString;
- (NSString *)createdDateString;
- (NSString *)deathDateString;

+ (NSString *)newsStatusStringFromStatus:(NewsStatus)status;
- (NSString *)newsStatusString;
+ (NSInteger)numberOfStatuses;

@end
