//
//  Message.m
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "News.h"

#import "NSDate+stringDate.h"

@implementation News

- (NSString *)priorityString {
    NSString *result = @"";
    switch (self.priority) {
        case NewsPriorityLow:
            result = @"низкий";
            break;
        case NewsPriorityNormal:
            result = @"средний";
            break;
        case NewsPriorityHight:
            result = @"высокий";
            break;
        default:
            break;
    }
    return  result;
}

- (NSString *)tagsString {
    NSString *result = @"";
    if ([self.tags count] > 0) {
        result = @"Теги: ";
        for (NSString *tag in self.tags) {
            result = [NSString stringWithFormat:@"%@%@, ",result,tag];
        }
        result = [result substringToIndex:[result length] - 2];
    }
    return result;
}

- (NSString *)createdDateString {
    return [self.createdDate stringDate];
}

- (NSString *)deathDateString {
    NSString *result;
    if (self.deathDate) {
        result = [self.deathDate stringDate];
    }
    else {
        result = @"никогда";
    }
    return result;
}

+ (NSString *)newsStatusStringFromStatus:(NewsStatus)status {
    switch (status) {
        case NewsStatusAccepted:
            return @"Добавленные в ЛН";
            break;
        case NewsStatusModeration:
            return @"ОМС";
            break;
        case NewsStatusRejected:
            return @"ЛОС";
        default:
            return @"";
            break;
    }
}

- (NSString *)newsStatusString {
    return [News newsStatusStringFromStatus:self.newsStatus];
}

+ (NSInteger)numberOfStatuses {
    return 3;
}

@end
