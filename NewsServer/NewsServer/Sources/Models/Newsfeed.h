//
//  NewsInfo.h
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedInfo.h"

@interface Newsfeed : NSObject

@property (nonatomic) NewsfeedInfo *newsfeedInfo;
@property (nonatomic) NSArray *importantNewsList;
@property (nonatomic) NSArray *usersNewsList;

- (NSArray *)usersNewsListWithTags:(NSArray *)tags;

@end
