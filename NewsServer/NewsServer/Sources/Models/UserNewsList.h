//
//  UserNewsList.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface UserNewsList : NSObject

@property (nonatomic) NSArray *acceptedNewsList;
@property (nonatomic) NSArray *moderatedNewsList;
@property (nonatomic) NSArray *rejectedNewsList;

@end
