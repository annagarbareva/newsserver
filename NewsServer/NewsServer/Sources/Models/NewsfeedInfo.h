//
//  NewsfeedInfo.h
//  NewsServer
//
//  Created by Sovelu on 24.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

typedef enum {
    NewsfeedTypeUnknown,
    NewsfeedTypeModerated,
    NewsfeedTypeNonModerated
}NewsfeedType;

@interface NewsfeedInfo : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic) NewsfeedType type;
@property (nonatomic, strong) NSString* authorName;
@property (nonatomic) NSUInteger newsCount;

@end
