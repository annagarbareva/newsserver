//
//  AcceptedNews.h
//  NewsServer
//
//  Created by Sovelu on 24.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface AcceptedNewsInNewsfeed : NSObject

@property (nonatomic) NSString *newsfeedName;
@property (nonatomic) NSArray *newsList;

@end
