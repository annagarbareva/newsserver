//
//  User.m
//  NewsServer
//
//  Created by Sovelu on 12.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "User.h"

#import "NSDate+stringDate.h"

@implementation User

- (NSString *)userStatusString {
    switch (self.status) {
        case UserStatusFull:
            return @"полный";
            break;
        case UserStatusLimited:
            return @"ограниченный";
        default:
            return @"";
            break;
    }
}

- (NSString *)timeRegisteredString {
    return [self.timeRegistered stringDate];
}

- (NSString *)banExitDateString {
    return [self.banExitDate stringDate];
}

@end
