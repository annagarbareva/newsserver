//
//  NewsInfo.m
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "Newsfeed.h"
#import "News.h"

@implementation Newsfeed

- (NSArray *)usersNewsListWithTags:(NSArray *)tags {
    NSMutableArray *result = [NSMutableArray new];
    for (News *news in self.usersNewsList) {
        if ([self doesNews:news containTags:tags]) {
            [result addObject:news];
        }
    }
    return result;
}

- (BOOL)doesNews:(News *)news containTags:(NSArray *)tags {
    NSArray *newsTags = news.tags;
    for (NSString *tag in tags) {
        if ([newsTags indexOfObject:tag] ==  NSNotFound) {
            return NO;
        }
    }
    return YES;
}

@end
