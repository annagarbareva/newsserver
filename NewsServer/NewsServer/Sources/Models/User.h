//
//  User.h
//  NewsServer
//
//  Created by Sovelu on 12.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

typedef enum{
    UserStatusFull,
    UserStatusLimited
}UserStatus;

@interface User : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDate *timeRegistered;
@property (nonatomic) BOOL isActive;
@property (nonatomic) BOOL isDeleted;
@property (nonatomic) UserStatus status;
@property (nonatomic) NSUInteger timeBanExit;
@property (nonatomic) NSUInteger banCount;
@property (nonatomic) NSDate *banExitDate;

- (NSString *)userStatusString;
- (NSString *)timeRegisteredString;
- (NSString *)banExitDateString;

@end
