//
//  AccessChecker.m
//  NewsServer
//
//  Created by Anna on 09/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "AccessChecker.h"
#import "ImportantNews.h"

@implementation AccessChecker

- (BOOL)canEditNews:(News *)news {
    if ([news isKindOfClass:[ImportantNews class]]) {
        return  NO;
    };
    if (news.newsStatus == NewsStatusRejected) {
        return NO;
    }
    NSString *userName = [self.loginStorage login];
    if (![userName isEqualToString: news.authorName]) {
        return NO;
    }
    return YES;
}

- (BOOL)canChangeNewsfeedNameInNews:(News *)news {
    if (news.newsStatus == NewsStatusModeration) {
        return NO;
    }
    return  YES;
}


- (void)hasUserFullStatusWithResponse:(void(^)(BOOL hasFullStatus, NSError *error))block {
    [self.userProvider requestUserInfo:^(User *info, NSError *error) {
        if (block) {
            if (!error) {
                BOOL hasAccess = info.status == UserStatusFull;
                block(hasAccess, error);
            }
            else {
               block(NO, error); 
            }
        }
    }];
}

- (BOOL)canDeleteNewsfeed:(NewsfeedInfo *)newsfeed {
    NSString *userName = [self.loginStorage login];
    NSString *creatorName = newsfeed.authorName;
    return  [userName isEqualToString:creatorName];
}


@end
