//
//  AccessChecker.h
//  NewsServer
//
//  Created by Anna on 09/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "News.h"
#import "NewsfeedInfo.h"

#import "LoginAndPasswordStorage.h"
#import "UserProviderProtocol.h"

@interface AccessChecker : NSObject

@property (nonatomic) InjectedClass(LoginAndPasswordStorage) loginStorage;
@property (nonatomic)InjectedProtocol(UserProviderProtocol) userProvider;

- (BOOL)canEditNews:(News *)news;

- (BOOL)canChangeNewsfeedNameInNews:(News *)news;

- (void)hasUserFullStatusWithResponse:(void(^)(BOOL hasFullStatus, NSError *error))block;

- (BOOL)canDeleteNewsfeed:(NewsfeedInfo *)newsfeed;

@end
