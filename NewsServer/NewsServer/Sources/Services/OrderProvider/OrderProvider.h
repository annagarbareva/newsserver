//
//  OrderProvider.h
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface OrderProvider : NSObject

- (NSArray *)orderedNewsListFromArray:(NSArray *)newsList;

@end
