//
//  OrderProvider.m
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "OrderProvider.h"

@implementation OrderProvider

- (NSArray *)orderedNewsListFromArray:(NSArray *)newsList {
    return [newsList sortedArrayUsingDescriptors:[NSArray arrayWithObjects:[NSSortDescriptor sortDescriptorWithKey:@"priority" ascending:NO],[NSSortDescriptor sortDescriptorWithKey:@"createdDate" ascending:NO], nil]];
}

@end
