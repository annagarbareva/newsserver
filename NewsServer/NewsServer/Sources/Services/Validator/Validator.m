//
//  InputChecker.m
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "Validator.h"
#import "NSCharacterSet+Cyrillic.h"

@implementation Validator

- (BOOL)isCorrectLogin:(NSString *)login {
    NSMutableCharacterSet *validCharacters = [NSMutableCharacterSet new];
    [validCharacters formUnionWithCharacterSet:[NSCharacterSet cyrillicSet]];
    [validCharacters formUnionWithCharacterSet:[NSCharacterSet alphanumericCharacterSet]];
    
    BOOL isCorrectLetters = [login stringByTrimmingCharactersInSet:validCharacters].length == 0;
    BOOL isCorrectLenght = [self isCorrectLenght:login];
    
    return isCorrectLenght && isCorrectLetters;
}

- (BOOL)isCorrectPassword:(NSString *)password {
    return [self isCorrectLenght:password];
}

- (BOOL)isCorrectNewsfeedName:(NSString *)newsfeed {
    NSMutableCharacterSet *validCharacters = [NSMutableCharacterSet new];
    [validCharacters formUnionWithCharacterSet:[NSCharacterSet cyrillicSet]];
    [validCharacters formUnionWithCharacterSet:[NSCharacterSet letterCharacterSet]];
    
    BOOL isCorrectLetters = [newsfeed stringByTrimmingCharactersInSet:validCharacters].length == 0;
    BOOL isCorrectLenght = [self isCorrectLenght:newsfeed];
    
    return isCorrectLenght && isCorrectLetters;
}

- (BOOL)isCorrectLenght:(NSString *)string {
    return string.length <= 50 && string.length > 0;
}

@end
