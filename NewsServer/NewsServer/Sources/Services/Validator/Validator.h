//
//  InputChecker.h
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//


@interface Validator : NSObject

- (BOOL)isCorrectLogin:(NSString *)login;
- (BOOL)isCorrectPassword:(NSString *)password;
- (BOOL)isCorrectNewsfeedName:(NSString *)newsfeed;

@end
