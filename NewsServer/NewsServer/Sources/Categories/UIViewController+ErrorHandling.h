//
//  UIViewController+ErrorHandling.h
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface UIViewController(ErrorHandling)

- (void)handleError:(NSError *)error;

@end
