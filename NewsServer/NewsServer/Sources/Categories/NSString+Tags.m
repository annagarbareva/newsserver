//
//  NSString+Tags.m
//  NewsServer
//
//  Created by Anna on 04/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NSString+Tags.h"

@implementation NSString(Tags)

- (NSArray *)tagsArray {
    if ([self length] > 0) {
        return [self componentsSeparatedByString:@", "];
    }
    return @[];
}

@end
