//
//  NSCharacterSet+Cyrillic.m
//  AstroZ
//
//  Created by Aleksey Garbarev on 12.05.14.
//  Copyright (c) 2014 Al Digit. All rights reserved.
//

#import "NSCharacterSet+Cyrillic.h"

@implementation NSCharacterSet (Cyrillic)

+ (id) cyrillicSet
{
    return [NSCharacterSet characterSetWithCharactersInString:@"абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"];
}

@end
