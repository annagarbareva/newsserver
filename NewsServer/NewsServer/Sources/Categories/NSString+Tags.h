//
//  NSString+Tags.h
//  NewsServer
//
//  Created by Anna on 04/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface NSString(Tags)

- (NSArray *)tagsArray;

@end
