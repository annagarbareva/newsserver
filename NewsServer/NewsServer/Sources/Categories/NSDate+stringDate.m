//
//  NSDate+stringDate.m
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NSDate+stringDate.h"

@implementation NSDate(stringDate)

- (NSString *)stringDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm"];
    return [formatter stringFromDate: self];
}

@end
