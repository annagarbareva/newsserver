//
//  NSCharacterSet+Cyrillic.h
//  AstroZ
//
//  Created by Aleksey Garbarev on 12.05.14.
//  Copyright (c) 2014 Al Digit. All rights reserved.
//

@interface NSCharacterSet (Cyrillic)

+ (id) cyrillicSet;

@end
