//
//  UIViewController+ErrorHandling.m
//  NewsServer
//
//  Created by Anna on 18/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UIViewController+ErrorHandling.h"

@implementation UIViewController(ErrorHandling)

- (void)handleError:(NSError *)error {
    if (error) {
        [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
    }
}

@end
