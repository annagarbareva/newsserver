//
//  UIViewController+RefreshContolLabel.h
//  NewsServer
//
//  Created by Anna on 19/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface UIViewController(RefreshContolLabel)

- (NSAttributedString *)attributedLabelToRefreshControl;

@end
