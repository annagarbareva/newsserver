//
//  NSDate+stringDate.h
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface NSDate(stringDate)

- (NSString *)stringDate;

@end
