//
//  UIViewController+RefreshContolLabel.m
//  NewsServer
//
//  Created by Anna on 19/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UIViewController+RefreshContolLabel.h"

@implementation UIViewController(RefreshContolLabel)

- (NSAttributedString *)attributedLabelToRefreshControl {

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d, h:mm a"];
    NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
    NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor blackColor]forKey:NSForegroundColorAttributeName];
    return [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];

}

@end
