//
//  CoreComponentsAssembly.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "CoreComponentsAssembly.h"

#import "TRCConnectionAFNetworking.h"
#import "TRCConnectionMock.h"

#import "TRCValueTransformerNewsfeedType.h"
#import "TRCValueTransformerNewsPriority.h"
#import "TRCValueTransformerDate.h"
#import "TRCValueTransformerUserStatus.h"

#import "TRCObjectMapperNewsfeedInfo.h"
#import "TRCObjectMapperNews.h"
#import "TRCObjectMapperUser.h"
#import "TRCObjectMapperAcceptedNewsInNewsfeed.h"
#import "TRCObjectMapperImportantNews.h"

#import "ErrorHandler.h"
#import "TRCConnectionTokenWriter.h"
#import "SessionManager.h"

#import "NewsfeedProviderProtocol.h"
#import "NewsfeedProvider.h"

#import "NewsProviderProtocol.h"
#import "NewsProvider.h"

#import "UserProviderProtocol.h"
#import "UserProvider.h"

#import "DebugProviderProtocol.h"
#import "DebugProvider.h"

#import "LoginAndPasswordStorage.h"
#import "AccessChecker.h"
#import "OrderProvider.h"
#import "Validator.h"

@implementation CoreComponentsAssembly

- (TyphoonRestClient *)restClient {
    return [TyphoonDefinition withClass:[TyphoonRestClient class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
        [definition injectProperty:@selector(connection) with:[self connectionTokenWriter]];
       
        [definition injectMethod:@selector(registerValueTransformer:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCValueTransformerNewsfeedType new]];
            [method injectParameterWith:@"{newsfeed-type}"];
        }];
        
        [definition injectMethod:@selector(registerValueTransformer:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCValueTransformerNewsPriority new]];
            [method injectParameterWith:@"{news-priority}"];
        }];
        
        [definition injectMethod:@selector(registerValueTransformer:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCValueTransformerDate new]];
            [method injectParameterWith:@"{date}"];
        }];
        
        [definition injectMethod:@selector(registerValueTransformer:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCValueTransformerUserStatus new]];
            [method injectParameterWith:@"{user-status}"];
        }];
        
        [definition injectMethod:@selector(registerObjectMapper:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCObjectMapperNewsfeedInfo new]];
            [method injectParameterWith:@"{newsfeed-info}"];
        }];
        
        [definition injectMethod:@selector(registerObjectMapper:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCObjectMapperNews new]];
            [method injectParameterWith:@"{news}"];
        }];
        
        [definition injectMethod:@selector(registerObjectMapper:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCObjectMapperUser new]];
            [method injectParameterWith:@"{user}"];
        }];
        
        [definition injectMethod:@selector(registerObjectMapper:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCObjectMapperAcceptedNewsInNewsfeed new]];
            [method injectParameterWith:@"{news-accepted-in-newsfeed}"];
        }];
        
        [definition injectMethod:@selector(registerObjectMapper:forTag:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[TRCObjectMapperImportantNews new]];
            [method injectParameterWith:@"{news-important}"];
        }];
        
        [definition injectProperty:@selector(errorParser) with:[ErrorHandler new]];
    }];
}

#pragma mark - providers/managers

- (id<NewsfeedProviderProtocol>)newsfeedProvider {
    return [TyphoonDefinition withClass:[NewsfeedProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id)sessionManager
{
    return [TyphoonDefinition withClass:[SessionManager class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id<NewsProviderProtocol>)newsProvider {
    return [TyphoonDefinition withClass:[NewsProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id<UserProviderProtocol>)userProvider {
    return [TyphoonDefinition withClass:[UserProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id<DebugProviderProtocol>)debugProvider {
    return [TyphoonDefinition withClass:[DebugProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

#pragma mark - connection

- (id)afnetworkingConnection {
    return [TyphoonDefinition withClass:[TRCConnectionAFNetworking class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithBaseUrl:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSURL URLWithString:@"http://localhost:8888"]]; //http://82.200.2.53/
        }];
    }];
}

- (id)mockConnection {
    return [TyphoonDefinition withClass:[TRCConnectionMock class] configuration:^(TyphoonDefinition *definition) {
        [definition useInitializer:@selector(initWithBaseUrl:) parameters:^(TyphoonMethod *initializer) {
            [initializer injectParameterWith:[NSURL URLWithString:@"http://mock.com"]];
        }];
    }];
}


- (id)connectionLogger {
    return [TyphoonDefinition withClass:[TRCConnectionLogger class] configuration:^(TyphoonDefinition *definition) {
        [definition injectMethod:@selector(initWithConnection:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[self afnetworkingConnection]];
        }];
    }];
}

- (id)connectionTokenWriter {
    return [TyphoonDefinition withClass:[TRCConnectionTokenWriter class] configuration:^(TyphoonDefinition *definition) {
        [definition injectMethod:@selector(initWithConnection:) parameters:^(TyphoonMethod *method) {
            [method injectParameterWith:[self connectionLogger]];
        }];
    }];
}

#pragma mark -

- (id)passwordStorage {
    return [TyphoonDefinition withClass:[LoginAndPasswordStorage class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id)accessChecker {
    return [TyphoonDefinition withClass:[AccessChecker class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id)orderProvider {
    return [TyphoonDefinition withClass:[OrderProvider class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

- (id)validator {
    return [TyphoonDefinition withClass:[Validator class] configuration:^(TyphoonDefinition *definition) {
        definition.scope = TyphoonScopeSingleton;
    }];
}

@end
