//
//  ErrorUtils.m
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "ErrorUtils.h"

NSError *NSErrorWithFormat(NSString *format, ...) {
    va_list args;
    va_start(args, format);
    NSString *description = [[NSString alloc] initWithFormat:format arguments:args];
    va_end(args);
    return [NSError errorWithDomain:@"" code:0 userInfo:@{NSLocalizedDescriptionKey : description}];
}
