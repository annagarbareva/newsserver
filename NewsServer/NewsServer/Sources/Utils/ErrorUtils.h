//
//  ErrorUtils.h
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

NSError *NSErrorWithFormat(NSString *format, ...) NS_FORMAT_FUNCTION(1,2);

