//
//  SessionManager.m
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "SessionManager.h"
#import "Session.h"

#import "RequestToRegisterUser.h"
#import "RequestToLoginUser.h"
#import "RequestToDeleteUser.h"
#import "RequestToLogout.h"

NSString *UserDeletedNotification = @"UserDeletedNotification";

@implementation SessionManager

- (void)registerUser:(NSString*)userName withPassword:(NSString*)password responce:(void(^)(NSString *token, NSError *error))block {
    RequestToRegisterUser *request = [RequestToRegisterUser new];
    request.name = userName;
    request.password = password;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (!error) {
            self.currentSession = [Session new];
            self.currentSession.accessToken = result;
        }
        if (block) {
            block(result, error);
        }
    }];
}

- (void)deleteUser:(NSString*)userName withPassword:(NSString*)password responce:(WebServiceResponseBlock)responceBlock {
    RequestToDeleteUser *request = [RequestToDeleteUser new];
    request.name = userName;
    request.password = password;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (!error) {
            [[NSNotificationCenter defaultCenter]postNotificationName:UserDeletedNotification object:nil];
        }
        if (responceBlock) {
            responceBlock(error);
        }
    }];
}

- (void)loginWithUser:(NSString*)userName withPassword:(NSString*)password responce:(void(^)(NSString *token, NSError *error))block {
    RequestToLoginUser *request = [RequestToLoginUser new];
    request.name = userName;
    request.pasword = password;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (!error) {
            self.currentSession = [Session new];
            self.currentSession.accessToken = result;
        }        
        if (block) {
            block(result, error);
        }
    }];
}

- (void)logoutwithResponce:(WebServiceResponseBlock)responceBlock {
    RequestToLogout *request = [RequestToLogout new];
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responceBlock) {
            responceBlock(error);
        }
    }];
}

@end
