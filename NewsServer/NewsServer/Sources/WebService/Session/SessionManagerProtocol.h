//
//  SessionManagerProtocol.h
//  NewsServer
//
//  Created by Anna on 19/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

extern NSString *UserDeletedNotification;

typedef void(^WebServiceResponseBlock)(NSError *error);

@protocol SessionManagerProtocol <NSObject>

- (void)registerUser:(NSString*)userName withPassword:(NSString*)password responce:(void(^)(NSString *token, NSError *error))block;
- (void)deleteUser:(NSString*)userName withPassword:(NSString*)password responce:(WebServiceResponseBlock)responceBlock;
- (void)loginWithUser:(NSString*)userName withPassword:(NSString*)password responce:(void(^)(NSString *token, NSError *error))block;
- (void)logoutwithResponce:(WebServiceResponseBlock)responceBlock;

@end
