//
//  SessionManager.h
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "Session.h"
#import "TyphoonRestClient.h"
#import "SessionManagerProtocol.h"

@interface SessionManager : NSObject <SessionManagerProtocol>

@property (nonatomic) Session *currentSession;
@property (nonatomic) InjectedClass(TyphoonRestClient) restClient;

@end
