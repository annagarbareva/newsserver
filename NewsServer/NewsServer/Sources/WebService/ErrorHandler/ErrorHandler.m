//
//  ErrorHandler.m
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "ErrorHandler.h"
#import "ErrorUtils.h"

@implementation ErrorHandler

- (NSError *)errorFromResponseBody:(NSDictionary *)bodyObject headers:(NSDictionary *)headers status:(TRCHttpStatusCode)statusCode error:(NSError **)error {
    return NSErrorWithFormat(@"%@ status %@", bodyObject[@"statusString"], bodyObject[@"status"]);
}

- (BOOL)isErrorResponseBody:(id)bodyObject headers:(NSDictionary *)headers status:(TRCHttpStatusCode)statusCode{
    if ([bodyObject isKindOfClass:[NSDictionary class]]) {
        if (bodyObject[@"status"]) {
            return [bodyObject[@"status"]integerValue] != 200;
        }
    }
    return NO;
}

@end
