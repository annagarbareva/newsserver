//
//  RequestToEditNewsWithPriority.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToEditNewsWithPriority.h"

@implementation RequestToEditNewsWithPriority

- (NSString *)path {
    return @"/api/news/feeds/{newsfeedName}/messages/{newsID}/priority";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPut;
}

- (NSDictionary *)pathParameters {
    return @{
             @"newsfeedName" : self.news.newsfeedName,
             @"newsID" : self.news.newsID
             };
}

- (id)requestBody {
    return @{
            @"priority" : @(self.priority)
             };
}

@end
