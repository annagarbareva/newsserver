//
//  RequestToEditNewsWithPriority.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "News.h"

@interface RequestToEditNewsWithPriority : NSObject <TRCRequest>

@property (nonatomic) News *news;
@property (nonatomic) NewsPriority priority;

@end
