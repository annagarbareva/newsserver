//
//  RequestToGetNews.h
//  NewsServer
//
//  Created by Anna on 16/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface RequestToGetNews : NSObject <TRCRequest>

@property (nonatomic, strong) NSString *newsID;
@property (nonatomic, strong) NSString *newsfeedName;

@end
