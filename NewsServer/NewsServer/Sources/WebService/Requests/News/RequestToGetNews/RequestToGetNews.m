//
//  RequestToGetNews.m
//  NewsServer
//
//  Created by Anna on 16/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetNews.h"
#import "News.h"

@implementation RequestToGetNews

- (NSString *)path {
    return @"/api/news/feeds/{newsfeedName}/messages/{newsID}";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (NSDictionary *)pathParameters {
    return @{
             @"newsfeedName" : self.newsfeedName,
             @"newsID" : self.newsID
             };
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    News *news = [News new];
    news.newsfeedName = self.newsfeedName;
    news.subject = bodyObject[@"subject"];
    news.body = bodyObject[@"body"];
    news.priority = (NewsPriority)[bodyObject[@"priority"] integerValue];
    news.isPrivate = [bodyObject[@"privateMessage"]boolValue];
    news.tags = bodyObject[@"tags"];
    news.newsID = bodyObject[@"id"];
    news.createdDate = bodyObject[@"created"];
    news.deathDate = bodyObject[@"liveUntil"];
    news.newsStatus = (NewsStatus)[bodyObject[@"messageStatus"]integerValue];
    return news;
}

@end
