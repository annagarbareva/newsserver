//
//  RequestToEditNewsWithNewsfeedName.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "News.h"

@interface RequestToEditNewsWithNewsfeedName : NSObject <TRCRequest>

@property (nonatomic) News *news;
@property (nonatomic) NSString *newsfeedName;

@end
