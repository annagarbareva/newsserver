//
//  RequestToEditNewsWithNewsfeedName.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToEditNewsWithNewsfeedName.h"

@implementation RequestToEditNewsWithNewsfeedName

- (NSString *)path {
    return @"/api/news/feeds/{newsfeedName}/messages/{newsID}/move";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPut;
}

- (NSDictionary *)pathParameters {
    return @{
             @"newsfeedName" : self.news.newsfeedName,
             @"newsID" : self.news.newsID
             };
}

- (id)requestBody {
    return @{
             @"destinationName" : self.newsfeedName
             };
}

@end
