//
//  RequestToDeleteNews.h
//  NewsServer
//
//  Created by Sovelu on 16.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "News.h"

@interface RequestToDeleteNews : NSObject <TRCRequest>

@property (nonatomic) News *news;

@end
