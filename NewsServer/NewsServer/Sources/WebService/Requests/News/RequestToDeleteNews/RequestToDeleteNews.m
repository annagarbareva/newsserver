//
//  RequestToDeleteNews.m
//  NewsServer
//
//  Created by Sovelu on 16.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToDeleteNews.h"

@implementation RequestToDeleteNews

- (NSString *)path {
    return @"/api/news/feeds/{newsfeedName}/messages/{newsID}";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodDelete;
}

- (NSDictionary *)pathParameters {
    return @{
             @"newsfeedName" : self.news.newsfeedName,
             @"newsID" : self.news.newsID
             };
}

@end
