//
//  RequestToCreateNews.m
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToCreateNews.h"

@implementation RequestToCreateNews

- (NSString *)path {
    return @"/api/news/feeds/{name}/messages";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPost;
}

- (NSDictionary *)pathParameters {
    return @{
             @"name" : self.news.newsfeedName,
             };
}

- (id)requestBody {
    return @{
             @"subject" : self.news.subject,
             @"body" : self.news.body,
             @"priority" : @(self.news.priority),
             @"privateMessage" : @(self.news.isPrivate),
             @"tags" : self.news.tags,
             @"ttl" : ValueOrNull(self.news.ttl)
             };
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return @{
             @"id" : bodyObject[@"id"],
             @"messageStatus" : bodyObject[@"messageStatus"]
             };
}

@end
