//
//  RequestToCreateNews.h
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "News.h"

@interface RequestToCreateNews : NSObject <TRCRequest>

@property (nonatomic) News *news;

@end
