//
//  RequestToEditNewsWithBody.m
//  NewsServer
//
//  Created by Sovelu on 16.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToEditNewsWithSubjectWithBody.h"

@implementation RequestToEditNewsWithSubjectWithBody

- (NSString *)path {
    return @"/api/news/feeds/{newsfeedName}/messages/{newsID}";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPut;
}

- (NSDictionary *)pathParameters {
    return @{
             @"newsfeedName" : self.news.newsfeedName,
             @"newsID" : self.news.newsID
             };
}

- (id)requestBody {
    return @{
             @"newSubject" : self.subject,
             @"newBody" : self.body
             };
}

@end
