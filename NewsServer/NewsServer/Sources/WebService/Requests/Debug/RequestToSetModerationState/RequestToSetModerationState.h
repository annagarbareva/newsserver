//
//  RequestToSetModerationState.h
//  NewsServer
//
//  Created by Anna on 17/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface RequestToSetModerationState : NSObject <TRCRequest>

@property (nonatomic) BOOL isModerated;

@end
