//
//  RequestToSetModerationState.m
//  NewsServer
//
//  Created by Anna on 17/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToSetModerationState.h"

@implementation RequestToSetModerationState

- (NSString *)path {
    return @"/api/news/debug/moderation";
}

- (TRCRequestMethod)method {
    return  TRCRequestMethodPut;
}

- (NSDictionary *)requestHeaders {
    return @{
             @"moderate" : @(self.isModerated)
             };
}

@end
