//
//  RequestToGetUserList.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetUserList.h"

@implementation RequestToGetUserList

-(NSString *)path {
    return @"/api/news/users";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return bodyObject[@"userInfoList"];
}

@end
