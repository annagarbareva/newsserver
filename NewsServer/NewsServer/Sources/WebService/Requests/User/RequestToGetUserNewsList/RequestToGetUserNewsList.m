//
//  RequestToGetUserNewsList.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetUserNewsList.h"

#import "UserNewsList.h"
#import "AcceptedNewsInNewsfeed.h"
#import "News.h"

@implementation RequestToGetUserNewsList

-(NSString *)path {
    return @"/api/news/user/messages";
}

-(TRCRequestMethod)method {
    return TRCRequestMethodGet;
}


- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    UserNewsList *userNews = [UserNewsList new];
    NSArray *acceptedNewsGroupedByNewsfeed = bodyObject[@"acceptedMessageList"];
    NSMutableArray *acceptedNewsList = [NSMutableArray new];
    for (AcceptedNewsInNewsfeed *newsfeed in acceptedNewsGroupedByNewsfeed) {
        for (News *news in newsfeed.newsList) {
            news.newsfeedName = newsfeed.newsfeedName;
            [acceptedNewsList addObject:news];
        }
    }
    userNews.acceptedNewsList = acceptedNewsList;
    userNews.moderatedNewsList = bodyObject[@"moderateMessageList"];
    userNews.rejectedNewsList = bodyObject[@"rejectedMessageList"];
    return userNews;
}
@end
