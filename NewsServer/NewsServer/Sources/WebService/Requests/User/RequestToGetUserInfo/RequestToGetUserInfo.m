//
//  RequestToGetUserInfo.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetUserInfo.h"

@implementation RequestToGetUserInfo

- (NSString *)path {
    return @"/api/news/user";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    User *result = bodyObject[@"userInfo"];
    return result;
}

@end
