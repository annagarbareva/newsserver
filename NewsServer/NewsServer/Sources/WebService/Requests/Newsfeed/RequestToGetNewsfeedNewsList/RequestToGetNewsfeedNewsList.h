//
//  RequestToGetNewsfeedNewsList.h
//  NewsServer
//
//  Created by Sovelu on 18.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "NewsfeedInfo.h"

@interface RequestToGetNewsfeedNewsList : NSObject <TRCRequest>

@property (nonatomic) NewsfeedInfo *newsfeedInfo;
@property (nonatomic) NSArray *tags;

@end
