//
//  RequestToGetNewsfeedNewsList.m
//  NewsServer
//
//  Created by Sovelu on 18.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetNewsfeedNewsList.h"
#import "Newsfeed.h"
#import "News.h"

@implementation RequestToGetNewsfeedNewsList

- (NSString *)path {
    return @"/api/news/feeds/{name}/messages";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (NSDictionary *)pathParameters {
    return @{
             @"name" : self.newsfeedInfo.name,
             @"tags" : ValueOrNull(self.tags)
             };
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    Newsfeed *newsfeed = [Newsfeed new];
    newsfeed.newsfeedInfo = self.newsfeedInfo;
    newsfeed.importantNewsList = bodyObject[@"veryImportantMessageList"];
    for (News *news in newsfeed.importantNewsList) {
        news.newsfeedName = self.newsfeedInfo.name;
    }
    newsfeed.usersNewsList = bodyObject[@"userMessageList"];
    for (News *news in newsfeed.usersNewsList) {
        news.newsfeedName = self.newsfeedInfo.name;
    }
    return newsfeed;
}

@end
