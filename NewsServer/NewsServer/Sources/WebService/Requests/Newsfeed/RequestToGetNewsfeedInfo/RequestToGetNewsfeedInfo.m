//
//  RequestToGetNewsfeedInfo.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetNewsfeedInfo.h"

@implementation RequestToGetNewsfeedInfo

- (NSString *)path {
    return @"/api/news/feeds/{name}";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (NSDictionary *)pathParameters {
    return @{
             @"name" : self.name
             };
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return bodyObject[@"feedInfo"];
}

@end
