//
//  RequestToGetNewsfeedList.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToGetNewsfeedList.h"

@implementation RequestToGetNewsfeedList

- (NSString *)path {
    return @"/api/news/feeds";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodGet;
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return bodyObject[@"feedInfoList"];
}

@end
