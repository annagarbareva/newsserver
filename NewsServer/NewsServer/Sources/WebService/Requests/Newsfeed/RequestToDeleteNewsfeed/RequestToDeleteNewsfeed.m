//
//  RequestToDeleteNewsfeed.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToDeleteNewsfeed.h"

@implementation RequestToDeleteNewsfeed

- (NSString *)path {
    return @"/api/news/feeds/{name}";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodDelete;
}

- (NSDictionary *)pathParameters {
    return @{
             @"name" : self.name
             };
}

@end
