//
//  RequestToCreateNewsfeed.h
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"
#import "Newsfeed.h"

@interface RequestToCreateNewsfeed : NSObject <TRCRequest>

@property (nonatomic) NSString *name;
@property (nonatomic) NewsfeedType type;

@end
