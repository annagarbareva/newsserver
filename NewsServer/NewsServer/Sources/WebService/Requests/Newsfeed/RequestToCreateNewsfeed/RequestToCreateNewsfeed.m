//
//  RequestToCreateNewsfeed.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToCreateNewsfeed.h"

@implementation RequestToCreateNewsfeed

- (NSString *)path {
    return @"/api/news/feeds/";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPost;
}

- (id)requestBody {
    return @{
             @"name" : self.name,
             @"type" : @(self.type)
             };
}

@end
