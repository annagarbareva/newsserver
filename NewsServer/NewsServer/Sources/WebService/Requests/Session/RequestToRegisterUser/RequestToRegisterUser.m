//
//  RequestToRegisterUser.m
//  NewsServer
//
//  Created by Anna on 19/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToRegisterUser.h"

@implementation RequestToRegisterUser

- (NSString *)path {
    return @"/api/news/users/";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPost;
}

- (id)requestBody {
    return @{
             @"name" : self.name,
             @"password" : self.password
             };
}

-(id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return bodyObject[@"token"];
}

@end
