//
//  RequestToRegisterUser.h
//  NewsServer
//
//  Created by Anna on 19/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface RequestToRegisterUser : NSObject <TRCRequest>

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *password;

@end
