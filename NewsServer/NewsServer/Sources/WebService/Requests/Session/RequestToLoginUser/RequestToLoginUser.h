//
//  RequestToLoginUser.h
//  NewsServer
//
//  Created by Anna on 20/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface RequestToLoginUser : NSObject <TRCRequest>

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *pasword;

@end
