//
//  RequestToLoginUser.m
//  NewsServer
//
//  Created by Anna on 20/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToLoginUser.h"

@implementation RequestToLoginUser

- (NSString *)path {
    return @"/api/news/session";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPost;
}

- (id)requestBody {
    return @{
             @"name" : self.name,
             @"password" : self.pasword
             };
}

- (id)responseProcessedFromBody:(NSDictionary *)bodyObject headers:(NSDictionary *)responseHeaders status:(TRCHttpStatusCode)statusCode error:(NSError *__autoreleasing *)parseError {
    return bodyObject[@"token"];
}

@end
