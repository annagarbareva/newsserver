//
//  RequestToLogout.m
//  NewsServer
//
//  Created by Sovelu on 06.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToLogout.h"

@implementation RequestToLogout

- (NSString *)path {
    return @"/api/news/session/";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodDelete;
}

@end
