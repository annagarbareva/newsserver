//
//  RequestToDeleteUser.m
//  NewsServer
//
//  Created by Anna on 02/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "RequestToDeleteUser.h"

@implementation RequestToDeleteUser

- (NSString *)path {
   return @"/api/news/users/{username}/unregister";
}

- (TRCRequestMethod)method {
    return TRCRequestMethodPut;
}

-(NSDictionary *)pathParameters {
    return @{
             @"username" : self.name
             };
}

- (id)requestBody {
    return @{
             @"password" : self.password
             };
}

@end
