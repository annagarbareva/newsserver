//
//  RequestToDeleteUser.h
//  NewsServer
//
//  Created by Anna on 02/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface RequestToDeleteUser : NSObject <TRCRequest>

@property (nonatomic) NSString *name;
@property (nonatomic) NSString *password;

@end
