//
//  TRCObjectMapperNewsfeed.h
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapper.h"

@interface TRCObjectMapperNewsfeedInfo : NSObject <TRCObjectMapper>

@end
