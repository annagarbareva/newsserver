//
//  TRCObjectMapperNewsfeed.m
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapperNewsfeedInfo.h"
#import "NewsfeedInfo.h"

@implementation TRCObjectMapperNewsfeedInfo

- (id)objectFromResponseObject:(NSDictionary *)dict error:(NSError **)error {
    NewsfeedInfo *newsfeedInfo = [NewsfeedInfo new];
    newsfeedInfo.name = dict[@"name"];
    newsfeedInfo.type = (NewsfeedType)[dict[@"type"] integerValue];
    newsfeedInfo.newsCount = [dict[@"messagesCount"] integerValue];
    newsfeedInfo.authorName = dict[@"creator"];
    return newsfeedInfo;
}

@end
