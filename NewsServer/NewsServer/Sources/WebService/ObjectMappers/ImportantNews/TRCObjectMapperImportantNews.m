//
//  TRCObjectMapperImportantNews.m
//  NewsServer
//
//  Created by Sovelu on 24.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapperImportantNews.h"
#import "ImportantNews.h"

@implementation TRCObjectMapperImportantNews

- (id)objectFromResponseObject:(NSDictionary *)responseObject error:(NSError *__autoreleasing *)error {
    ImportantNews *news = [ImportantNews new];
    news.newsID = responseObject[@"id"];
    news.subject = responseObject[@"subject"];
    news.body = responseObject[@"body"];
    news.createdDate = responseObject[@"created"];
    return news;
}

@end
