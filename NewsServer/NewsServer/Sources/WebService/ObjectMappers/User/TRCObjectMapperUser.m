//
//  TRCObjectMapperUser.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapperUser.h"
#import "User.h"

@implementation TRCObjectMapperUser

- (id)objectFromResponseObject:(NSDictionary *)dict error:(NSError **)error {
    User *user = [User new];
    user.name = dict[@"name"];
    user.timeRegistered = dict[@"timeRegistered"];
    user.isActive = dict[@"online"];
    user.isDeleted = dict[@"deleted"];
    user.status = (UserStatus)[dict[@"userStatus"] integerValue];
    user.timeBanExit = [dict[@"timeBanExit"] integerValue];
    user.banCount = [dict[@"banCount"]integerValue];
    user.banExitDate = [NSDate dateWithTimeInterval:user.timeBanExit/1000 sinceDate:user.timeRegistered];
    return user;
}

@end
