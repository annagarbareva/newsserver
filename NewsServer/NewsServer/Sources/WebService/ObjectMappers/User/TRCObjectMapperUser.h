//
//  TRCObjectMapperUser.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapper.h"

@interface TRCObjectMapperUser : NSObject <TRCObjectMapper>

@end
