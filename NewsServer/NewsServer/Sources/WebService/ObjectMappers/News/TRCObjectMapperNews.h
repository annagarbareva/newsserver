//
//  TRCObjectMapperNews.h
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapper.h"

@interface TRCObjectMapperNews : NSObject <TRCObjectMapper>

@end
