//
//  TRCObjectMapperNews.m
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapperNews.h"
#import "News.h"
#import "TRCRequest.h"

@implementation TRCObjectMapperNews

- (id)requestObjectFromObject:(News *)object error:(NSError **)error {
    return @{
             @"name" : object.newsfeedName,
             @"subject" : object.subject,
             @"body" : object.body,
             @"priority" : @(object.priority),
             @"privateMessage" : @(object.isPrivate),
             @"tags" : object.tags,
             @"ttl" : ValueOrNull(object.ttl)
             };
}
-(id)objectFromResponseObject:(NSDictionary *)responseObject error:(NSError *__autoreleasing *)error {

    News *result = [News new];
    result.newsID = responseObject[@"id"];
    result.newsfeedName = responseObject[@"name"];
    result.isPrivate = [responseObject[@"privateMessage"]boolValue];
    result.subject = responseObject[@"subject"];
    result.body = responseObject[@"body"];
    result.priority = (NewsPriority)[responseObject[@"priority"] integerValue];
    result.tags = responseObject[@"tags"];
    result.createdDate = responseObject[@"created"];
    result.deathDate = responseObject[@"liveUntil"];
    result.votesPro = [responseObject[@"votesPro"] integerValue];
    result.votesContra = [responseObject[@"votesContra"] integerValue];
    result.authorName = responseObject[@"creator"];
    return result;
}

@end
