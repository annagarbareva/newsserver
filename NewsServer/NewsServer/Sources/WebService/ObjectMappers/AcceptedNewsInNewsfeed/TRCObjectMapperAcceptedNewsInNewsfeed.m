//
//  TRCObjectMapperAcceptedNews.m
//  NewsServer
//
//  Created by Sovelu on 24.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCObjectMapperAcceptedNewsInNewsfeed.h"
#import "AcceptedNewsInNewsfeed.h"

@implementation TRCObjectMapperAcceptedNewsInNewsfeed


- (id)objectFromResponseObject:(NSDictionary *)responseObject error:(NSError *__autoreleasing *)error {
    AcceptedNewsInNewsfeed *news = [AcceptedNewsInNewsfeed new];
    news.newsfeedName = responseObject[@"name"];
    news.newsList = responseObject[@"messages"];
    return news;
}

@end
