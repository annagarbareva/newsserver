//
//  TRCConnectionMock.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TyphoonRestClient.h"

@interface TRCConnectionMock : TRCConnectionAFNetworking

- (void)registerResponseFromFile:(NSString *)fileName forRequestWithPath:(NSString *)path;

@end
