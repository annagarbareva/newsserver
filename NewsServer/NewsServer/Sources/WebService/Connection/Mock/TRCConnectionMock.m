//
//  TRCConnectionMock.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCConnectionMock.h"

@interface TRCConnectionMockResponseInfo : NSObject <TRCResponseInfo>
@property (nonatomic) NSHTTPURLResponse *response;
@property (nonatomic) NSData *responseData;
@end

@implementation TRCConnectionMockResponseInfo
@end

@implementation TRCConnectionMock {
    NSMutableDictionary *filenamesForPaths;
}

- (instancetype)initWithBaseUrl:(NSURL *)baseUrl
{
    self = [super initWithBaseUrl:baseUrl];
    if (self) {
        filenamesForPaths = [NSMutableDictionary new];
        [self setupDefaultResponses];
    }
    return self;
}

- (id<TRCProgressHandler>)sendRequest:(NSURLRequest *)request withOptions:(id<TRCConnectionRequestSendingOptions>)options completion:(TRCConnectionCompletion)completion {
    NSDictionary *dict = filenamesForPaths;
    NSString *path = [dict objectForKey:request.URL.path];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:path ofType:nil];
    NSError *error = nil;
    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:JSONData options:kNilOptions error:&error];
    
    TRCConnectionMockResponseInfo *info = [TRCConnectionMockResponseInfo new];
    info.responseData = JSONData;
    NSString *fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil][NSFileSize] description];
    NSDictionary *responseHeaders = @{
                                      @"Server": @"TRCConnectionMock",
                                      @"Content-Type": @"application/json; charset=utf-8",
                                      @"Connection":@"Close",
                                      @"Content-Length": fileSize
                                      };
    info.response = [[NSHTTPURLResponse alloc] initWithURL:request.URL statusCode:200 HTTPVersion:@"1.1" headerFields:responseHeaders];
    if (completion) {
        completion(dictionary, nil, info);
    }
    return nil;
}

- (void)registerResponseFromFile:(NSString *)fileName forRequestWithPath:(NSString *)path {
    filenamesForPaths[path] = fileName;
}

- (void)setupDefaultResponses {
    [self registerResponseFromFile:@"NewsfeedListMock.json" forRequestWithPath:@"/api/news/get_news_list"];
    [self registerResponseFromFile:@"NewsfeedInfoMock.json" forRequestWithPath:@"/api/news/get_news_info"];
}


///api/news/register
///api/news/unregister
///api/news/login
///api/news/logout
///api/news/get_user_state
///api/news/get_users
///api/news/create_news
///api/news/rename_news
///api/news/delete_news
///api/news/get_news_info
//
///api/news/add_message
///api/news/get_message
///api/news/delete_message
///api/news/edit_message
///api/news/change_message_priority
///api/news/move_message
///api/news/get_news_message_list
///api/news/get_user_message_list


@end
