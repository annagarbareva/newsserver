//
//  TRCConnectionManager.h
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "SessionManager.h"
#import "TRCConnectionBaseProxy.h"

@interface TRCConnectionTokenWriter : TRCConnectionBaseProxy

@property (nonatomic) InjectedClass(SessionManager)  sessionManager;

@end
