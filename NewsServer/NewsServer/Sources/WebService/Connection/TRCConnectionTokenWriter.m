//
//  TRCConnectionManager.m
//  NewsServer
//
//  Created by Sovelu on 14.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCConnectionTokenWriter.h"
#import "Session.h"

@implementation TRCConnectionTokenWriter

- (NSMutableURLRequest *)requestWithOptions:(id<TRCConnectionRequestCreationOptions>)options error:(NSError **)requestComposingError
{
    NSMutableURLRequest *request = [self.connection requestWithOptions:options error:requestComposingError];
    if (self.sessionManager.currentSession) {
        [request setValue:self.sessionManager.currentSession.accessToken forHTTPHeaderField:@"token"];
    }
    return  request;
}


@end
