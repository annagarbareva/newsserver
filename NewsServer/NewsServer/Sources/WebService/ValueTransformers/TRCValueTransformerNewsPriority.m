//
//  TRCValueTransformerNewsType.m
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCValueTransformerNewsPriority.h"
#import "News.h"
#import "ErrorUtils.h"

@implementation TRCValueTransformerNewsPriority

- (id)objectFromResponseValue:(NSString *)responseValue error:(NSError **)error {
    if ([responseValue isEqualToString:@"HIGH"]) {
        return @(NewsPriorityHight);
    }
    else if ([responseValue isEqualToString:@"NORMAL"]) {
        return @(NewsPriorityNormal);
    }
    else if ([responseValue isEqualToString:@"LOW"]) {
        return @(NewsPriorityLow);
    }
    if (error) {
        *error = NSErrorWithFormat(@"Can't parse news priopity '%@'", responseValue);
    }
    return @(NewsPriorityNormal);
}

- (id)requestValueFromObject:(NSNumber *)object error:(NSError **)error {
    NewsPriority priority = (NewsPriority)[object integerValue];
    NSString *result = nil;
    switch (priority) {
        case NewsPriorityHight:
            result = @"HIGH";
            break;
        case NewsPriorityNormal:
            result = @"NORMAL";
            break;
        case NewsPriorityLow:
            result = @"LOW";
            break;
        default:
            if (error) {
                *error = NSErrorWithFormat(@"Incorrect news ptiopity'%@'",object);
            }
            break;
            
    }
    return result;
}


@end
