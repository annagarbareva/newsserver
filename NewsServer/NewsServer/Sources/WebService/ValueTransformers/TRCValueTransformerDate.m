//
//  TRCValueTransformerDate.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCValueTransformerDate.h"

@implementation TRCValueTransformerDate

- (id)objectFromResponseValue:(NSNumber *)responseValue error:(NSError **)error {
    return [NSDate dateWithTimeIntervalSince1970:[responseValue doubleValue]/1000.];
}

- (id)requestValueFromObject:(NSDate *)object error:(NSError **)error {
    return [[NSNumber alloc]initWithInteger:object.timeIntervalSince1970];
}

-(TRCValueTransformerType)externalTypes {
    return TRCValueTransformerTypeNumber;
}


@end
