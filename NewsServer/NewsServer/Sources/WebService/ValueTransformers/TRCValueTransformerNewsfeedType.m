//
//  TRCValueTransformerNewsfeedType.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCValueTransformerNewsfeedType.h"
#import "Newsfeed.h"
#import "TRCUtils.h"
#import "ErrorUtils.h"
@implementation TRCValueTransformerNewsfeedType

- (id)objectFromResponseValue:(NSString *)responseValue error:(NSError **)error {
    if ([responseValue isEqualToString:@"MODERATED"]) {
        return @(NewsfeedTypeModerated);
    }
    else if ([responseValue isEqualToString:@"NONMODERATED"]) {
        return @(NewsfeedTypeNonModerated);
    }
    if (error) {
        *error = NSErrorWithFormat(@"Can't parse newsfeed type '%@'", responseValue);
    }
    return @(NewsfeedTypeUnknown);
}

- (id)requestValueFromObject:(NSNumber *)object error:(NSError **)error {
    NewsfeedType type = (NewsfeedType)[object integerValue];
    NSString *result = nil;
    switch (type) {
        case NewsfeedTypeModerated:
            result = @"MODERATED";
            break;
        case NewsfeedTypeNonModerated:
            result = @"NONMODERATED";
            break;
        default:
            if (error) {
                *error = NSErrorWithFormat(@"Incorrect newsfeed type '%@'",object);
            }
            break;
            
    }
    return result;
}
@end
