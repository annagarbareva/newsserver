//
//  TRCValueTransformerUserStatus.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "TRCValueTransformerUserStatus.h"
#import "User.h"
#import "ErrorUtils.h"

@implementation TRCValueTransformerUserStatus

- (id)objectFromResponseValue:(NSString *)responseValue error:(NSError **)error {
    if ([responseValue isEqualToString:@"FULL"]) {
        return @(UserStatusFull);
    }
    else if ([responseValue isEqualToString:@"LIMITED"]) {
        return @(UserStatusLimited);
    }
    if (error) {
        *error = NSErrorWithFormat(@"Can't parse user status '%@'", responseValue);
    }
    return @(UserStatusFull);
}

- (id)requestValueFromObject:(NSNumber *)object error:(NSError **)error {
    UserStatus status = (UserStatus)[object integerValue];
    NSString *result = nil;
    switch (status) {
        case UserStatusFull:
            result = @"FULL";
            break;
        case UserStatusLimited:
            result = @"LIMITED";
            break;
        default:
            if (error) {
                *error = NSErrorWithFormat(@"Incorrect user status type '%@'",object);
            }
            break;
            
    }
    return result;
}

@end
