//
//  NewsProvider.m
//  NewsServer
//
//  Created by Anna on 15/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsProvider.h"

#import "RequestToCreateNews.h"
#import "RequestToDeleteNews.h"
#import "RequestToEditNewsWithSubjectWithBody.h"
#import "RequestToEditNewsWithPriority.h"
#import "RequestToEditNewsWithNewsfeedName.h"
#import "RequestToGetNews.h"

NSString *NewsCreatedNotification = @"NewsCreatedNotification";
NSString *NewsEditedNotification = @"NewsEditedNotification";
NSString *NewsMovedNotification = @"NewsMovedNotification";

@implementation NewsProvider

- (void)createNews:(News *)news responce:(void(^)(NSString *newsID, NewsStatus status, NSError *error))block {
    RequestToCreateNews *request = [RequestToCreateNews new];
    request.news = news;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (block) {
            NSDictionary *res = (NSDictionary *)result;
            block(res[@"id"], (NewsStatus)[res[@"messageStatus"]integerValue], error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsCreatedNotification object:nil];
            }
        }
    }];
}

- (void)deleteNews:(News *)news responce:(WebServiceResponseBlock)responceBlock {
    RequestToDeleteNews *request = [RequestToDeleteNews new];
    request.news = news;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responceBlock) {
            responceBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsMovedNotification object:nil];
            }
        }
    }];
}

- (void)editNews:(News *)news withSubject: (NSString *)subject withBody:(NSString*)body responce:(WebServiceResponseBlock)responceBlock {
    RequestToEditNewsWithSubjectWithBody *request = [RequestToEditNewsWithSubjectWithBody new];
    request.news = news;
    request.subject = subject;
    request.body = body;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responceBlock) {
            responceBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsEditedNotification object:nil];
            }
        }
    }];
}

- (void)editNews:(News *)news withPriority:(NewsPriority)priority responce:(WebServiceResponseBlock)responceBlock {
    RequestToEditNewsWithPriority *request = [RequestToEditNewsWithPriority new];
    request.news = news;
    request.priority = priority;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responceBlock) {
            responceBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsEditedNotification object:nil];
            }
        }
    }];
}

- (void)editNews:(News *)news withNewsfeedName:(NSString*)newsfeedName responce:(WebServiceResponseBlock)responceBlock {
    RequestToEditNewsWithNewsfeedName *request = [RequestToEditNewsWithNewsfeedName new];
    request.news = news;
    request.newsfeedName = newsfeedName;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responceBlock) {
            responceBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsMovedNotification object:nil];
            }
        }
    }];
}

- (void)requestNewsWithNewsID:(NSString *)newsID newsfeedName:(NSString *)newsfeedName responce:(void(^)(News *news, NSError *error))block {
    RequestToGetNews *request = [RequestToGetNews new];
    request.newsID = newsID;
    request.newsfeedName = newsfeedName;
    [self.restClient sendRequest:request completion:block];
}

@end
