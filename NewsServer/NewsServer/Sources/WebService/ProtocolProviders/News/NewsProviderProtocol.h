//
//  NewsProviderProtocol.h
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "News.h"

extern NSString *NewsCreatedNotification;
extern NSString *NewsEditedNotification;
extern NSString *NewsMovedNotification;

typedef void(^WebServiceResponseBlock)(NSError *error);

@protocol NewsProviderProtocol <NSObject>

- (void)createNews:(News *)news responce:(void(^)(NSString *newsID, NewsStatus status, NSError *error))block;
- (void)deleteNews:(News *)news responce:(WebServiceResponseBlock)responceBlock;
- (void)editNews:(News *)news withSubject: (NSString *)subject withBody:(NSString*)body responce:(WebServiceResponseBlock)responceBlock;
- (void)editNews:(News *)news withPriority:(NewsPriority)priority responce:(WebServiceResponseBlock)responceBlock;
- (void)editNews:(News *)news withNewsfeedName:(NSString*)newsfeedName responce:(WebServiceResponseBlock)responceBlock;
- (void)requestNewsWithNewsID:(NSString *)newsID newsfeedName:(NSString *)newsfeedName responce:(void(^)(News *news, NSError *error))block;

@end