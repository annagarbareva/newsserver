//
//  NewsfeedProvider.m
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProvider.h"
#import "TyphoonRestClient.h"

#import "RequestToCreateNewsfeed.h"
#import "RequestToDeleteNewsfeed.h"
#import "RequestToGetNewsfeedInfo.h"
#import "RequestToGetNewsfeedList.h"
#import "RequestToGetNewsfeedNewsList.h"

NSString *NewsfeedCreatedNotification = @"NewsfeedCreatedNotification";
NSString *NewsfeedDeletedNotification = @"NewsfeedDeletedNotification";

@implementation NewsfeedProvider

- (void)createNewsfeed:(NSString *)name type:(NewsfeedType)type response:(WebServiceResponseBlock)responseBlock {

    RequestToCreateNewsfeed *request = [RequestToCreateNewsfeed new];
    request.name = name;
    request.type = type;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responseBlock) {
            responseBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsfeedCreatedNotification object:nil];
            }
        }
    }];
}

- (void)deleteNewsfeed:(NSString *)name response:(WebServiceResponseBlock)responseBlock {
    RequestToDeleteNewsfeed *request = [RequestToDeleteNewsfeed new];
    request.name = name;
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (responseBlock) {
            responseBlock(error);
            if (!error) {
                [[NSNotificationCenter defaultCenter]postNotificationName:NewsfeedDeletedNotification object:nil];
            }
        }
    }];
}

- (void)requestNewsfeedInfo:(NSString *)name response:(void(^)(NewsfeedInfo *info, NSError *error))block {
    RequestToGetNewsfeedInfo *request = [RequestToGetNewsfeedInfo new];
    request.name = name;
    [self.restClient sendRequest:request completion:block];
}

- (void)requestNewsfeedList:(void(^)(NSArray *newsfeeds, NSError *))block {
    RequestToGetNewsfeedList *request = [RequestToGetNewsfeedList new];
    [self.restClient sendRequest:request completion:block];
}

- (void)requestNewsfeedNewsList:(NewsfeedInfo *)newsfeedInfo tags:(NSArray *)tags response: (void(^)(Newsfeed *newsfeed, NSError *error))block {
    RequestToGetNewsfeedNewsList *request = [RequestToGetNewsfeedNewsList new];
    request.newsfeedInfo = newsfeedInfo;
    request.tags = tags;    
    [self.restClient sendRequest:request completion:block];
}

@end
