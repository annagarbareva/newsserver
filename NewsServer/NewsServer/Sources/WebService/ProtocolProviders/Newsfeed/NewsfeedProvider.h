//
//  NewsfeedProvider.h
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProviderProtocol.h"
#import "TyphoonRestClient.h"

@interface NewsfeedProvider : NSObject <NewsfeedProviderProtocol>

@property (nonatomic, strong) InjectedClass(TyphoonRestClient) restClient;

@end
