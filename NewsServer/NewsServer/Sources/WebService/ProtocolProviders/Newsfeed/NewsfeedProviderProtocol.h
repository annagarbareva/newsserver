//
//  NewsfeedProviderProtocol.h
//  NewsServer
//
//  Created by Sovelu on 13.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedInfo.h"
#import "Newsfeed.h"

extern NSString *NewsfeedCreatedNotification;
extern NSString *NewsfeedDeletedNotification;

typedef void(^WebServiceResponseBlock)(NSError *error);

@protocol NewsfeedProviderProtocol <NSObject>

- (void)createNewsfeed:(NSString*)name type:(NewsfeedType)type response:(WebServiceResponseBlock)responseBlock;
- (void)deleteNewsfeed:(NSString*)name response:(WebServiceResponseBlock)responseBlock;
- (void)requestNewsfeedInfo:(NSString*)name response:(void(^)(NewsfeedInfo *info, NSError *error))block;
- (void)requestNewsfeedList:(void(^)(NSArray *newsfeeds, NSError *error))block;
- (void)requestNewsfeedNewsList:(NewsfeedInfo *)newsfeedInfo tags:(NSArray *)tags response: (void(^)(Newsfeed *newsfeed, NSError *error))block;

@end


