//
//  UserProviderProtocol.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "User.h"
#import "UserNewsList.h"

typedef void(^WebServiceResponseBlock)(NSError *error);

@protocol UserProviderProtocol <NSObject>

- (void)requestUserInfo:(void(^)(User *info, NSError *error))block;
- (void)requestUserList:(void(^)(NSArray *users, NSError *error))block;
- (void)requestUserNewsList:(void(^)(UserNewsList *newsList, NSError *error))block;

@end
