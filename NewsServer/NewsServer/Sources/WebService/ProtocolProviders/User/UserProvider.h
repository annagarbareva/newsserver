//
//  UserProvider.h
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserProviderProtocol.h"
#import "TyphoonRestClient.h"


@interface UserProvider : NSObject <UserProviderProtocol>

@property (nonatomic, strong) InjectedClass(TyphoonRestClient) restClient;

@end
