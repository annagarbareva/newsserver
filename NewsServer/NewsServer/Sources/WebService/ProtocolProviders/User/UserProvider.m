//
//  UserProvider.m
//  NewsServer
//
//  Created by Sovelu on 17.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserProvider.h"

#import "RequestToGetUserInfo.h"
#import "RequestToGetUserList.h"
#import "RequestToGetUserNewsList.h"

@implementation UserProvider

- (void)requestUserInfo:(void(^)(User *info, NSError *error))block {
    RequestToGetUserInfo *request = [RequestToGetUserInfo new];
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (block) {
            block(result,error);
        }
    }];
}

- (void)requestUserList:(void(^)(NSArray *users, NSError *error))block {
    RequestToGetUserList *request = [RequestToGetUserList new];
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (block) {
            block(result, error);
        }
    }];
}

- (void)requestUserNewsList:(void(^)(UserNewsList *newsList, NSError *error))block {
    RequestToGetUserNewsList *request = [RequestToGetUserNewsList new];
    [self.restClient sendRequest:request completion:^(id result, NSError *error) {
        if (block) {
            block(result, error);
        }
    }];
}

@end
