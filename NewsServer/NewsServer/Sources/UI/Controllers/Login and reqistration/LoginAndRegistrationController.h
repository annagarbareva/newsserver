//
//  LoginAndRegistrationController.h
//  NewsServer
//
//  Created by Anna on 19/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "SessionManager.h"
#import "LoginAndPasswordStorage.h"
#import "Validator.h"

@interface LoginAndRegistrationController : UIViewController

@property (nonatomic, strong) InjectedClass(SessionManager) sessionManager;
@property (nonatomic, strong) InjectedClass(LoginAndPasswordStorage) passwordStorage;

@property (nonatomic) InjectedClass(Validator) validator;

@end
