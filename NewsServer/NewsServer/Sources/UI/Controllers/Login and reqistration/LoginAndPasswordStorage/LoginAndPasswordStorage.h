//
//  PasswordStorage.h
//  NewsServer
//
//  Created by Sovelu on 06.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

@interface LoginAndPasswordStorage : NSObject

- (void)saveLogin:(NSString *)login password:(NSString *)password;
- (NSString *)password;
- (NSString *)login;
- (void)deleteLoginAndPassword;

@end
