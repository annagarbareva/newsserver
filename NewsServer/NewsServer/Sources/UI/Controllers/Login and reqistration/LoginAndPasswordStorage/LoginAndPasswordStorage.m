//
//  PasswordStorage.m
//  NewsServer
//
//  Created by Sovelu on 06.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "LoginAndPasswordStorage.h"

@implementation LoginAndPasswordStorage

- (void)saveLogin:(NSString *)login password:(NSString *)password {
    [[NSUserDefaults standardUserDefaults] setObject:login forKey:@"Login"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (NSString *)password {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Password"];
}

- (NSString *)login {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"Login"];
}

- (void)deleteLoginAndPassword {
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"Login"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

@end
