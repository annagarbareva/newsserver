//
//  LoginAndRegistrationController.m
//  NewsServer
//
//  Created by Anna on 19/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "LoginAndRegistrationController.h"

@interface LoginAndRegistrationController ()

@property (weak, nonatomic) IBOutlet UITextField *registrationLogin;
@property (weak, nonatomic) IBOutlet UITextField *registrationPassword;
@property (weak, nonatomic) IBOutlet UITextField *repeatedPassword;

@property (weak, nonatomic) IBOutlet UITextField *login;
@property (weak, nonatomic) IBOutlet UITextField *password;

- (IBAction)onRegister:(id)sender;

- (IBAction)onEnter:(id)sender;

@end

@implementation LoginAndRegistrationController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self clearFields];
}

- (void)clearFields {
    self.registrationLogin.text = @"";
    self.registrationPassword.text = @"";
    self.repeatedPassword.text = @"";
    self.login.text = @"";
    self.password.text = @"";
}

#pragma mark - IBActions

- (IBAction)onRegister:(id)sender {
    if ([self areCorrectLogin:self.registrationLogin.text password:self.registrationPassword.text]) {
        if ([self isPaswordReEntered]) {
            [self sendRequestToRegister];
        }
        else {
            [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:@"Неверно повторили пароль" delegate:nil cancelButtonTitle:@"Ок" otherButtonTitles: nil]show];
        }
    }
}

- (BOOL)areCorrectLogin:(NSString *)login password:(NSString *)password {
    if (![self.validator isCorrectLogin:login]) {
        [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:@"Логин может содержать только цифры и латинские и русские буквы, а его длина  должна быть от 1 до 50 символов" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
        return NO;
    }
    else if (![self.validator isCorrectPassword:password]) {
        [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:@"Длина пароля должна быть от 1 до 50 символов" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil]show];
        return NO;
    }
    return YES;
}

- (BOOL)isPaswordReEntered {
    return [self.registrationPassword.text isEqualToString:self.repeatedPassword.text];
}

- (void)sendRequestToRegister {
    [self.sessionManager registerUser:self.registrationLogin.text withPassword:self.registrationPassword.text responce:^(NSString *token, NSError *error) {
        if (!error) {
            [self successResponseWithToken:token];
            [self.passwordStorage saveLogin:self.registrationLogin.text password:self.registrationPassword.text];
        }
        else {
            [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
        }
    }];
}

- (IBAction)onEnter:(id)sender {
    if ([self areCorrectLogin:self.login.text password:self.password.text]) {
        [self.sessionManager loginWithUser:self.login.text withPassword:self.password.text responce:^(NSString *token, NSError *error) {
            if (!error) {
                [self successResponseWithToken:token];
                [self.passwordStorage saveLogin:self.login.text password:self.password.text];
            }
            else {
                [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil] show];
            }
        }];
    }
}

- (void)successResponseWithToken:(NSString *)token {
    self.sessionManager.currentSession.accessToken = token;
    UIViewController *destinationContr = [self.storyboard instantiateViewControllerWithIdentifier:@"UINavigationController"];
    [self showViewController:destinationContr sender:self];
}

@end
