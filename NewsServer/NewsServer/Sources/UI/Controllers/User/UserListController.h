//
//  UserListController.h
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserProviderProtocol.h"

@interface UserListController : UITableViewController

@property (nonatomic) InjectedProtocol(UserProviderProtocol) userProvider;

@end
