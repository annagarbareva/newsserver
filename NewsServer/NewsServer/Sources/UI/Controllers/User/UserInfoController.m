//
//  UserInfoController.m
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserInfoController.h"

@interface UserInfoController ()

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *registrationDate;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *banExitDate;
@property (weak, nonatomic) IBOutlet UILabel *banCount;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *banExitDateHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusBanExitDateDistance;

@end

@implementation UserInfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.userProvider requestUserInfo:^(User *info, NSError *error) {
        if (!error) {
            [self setTextsToLabelsFromUserInfo:info];
        }
    }];
}

- (void)setTextsToLabelsFromUserInfo:(User *)user {
    self.name.text = [NSString stringWithFormat:@"Имя: %@", user.name];
    self.registrationDate.text = [NSString stringWithFormat:@"Дата регистрации: %@", [user timeRegisteredString]];
    self.status.text = [NSString stringWithFormat:@"Статус: %@", [user userStatusString]];
    [self setTextToTimeBanExitLabelWithUserInfo:user];
    self.banCount.text = [NSString stringWithFormat:@"Число банов: %lu раз", (unsigned long)user.banCount];
}

- (void)setTextToTimeBanExitLabelWithUserInfo:(User *)info {
    if (info.timeBanExit) {
        self.banExitDate.text = [NSString stringWithFormat:@"Время окончания режима ограниченного доступа: %@", [info banExitDateString]];
    }
    else {
        self.banExitDate.text = @"";
        self.statusBanExitDateDistance.constant = 0;
        self.banExitDateHeight.constant = 0;
    }
}

@end
