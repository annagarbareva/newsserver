//
//  UserListController.m
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserListController.h"

#import "UserTableViewCell.h"

@interface UserListController ()

@property (nonatomic) NSArray *users;

@end

@implementation UserListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNib];
    [self setTableViewSettings];
    [self receiveUserList];
}

- (void)registerNib {
    UINib *nib = [UINib nibWithNibName:@"UserTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:[UserTableViewCell identifier]];
}

- (void)receiveUserList {
    [self.userProvider requestUserList:^(NSArray *users, NSError *error) {
        if (!error) {
            self.users = users;
            [self.tableView reloadData];
        }
    }];
}

- (void)setTableViewSettings {
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
}

#pragma mark - Table view datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.users count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[UserTableViewCell identifier]];
    if (!cell) {
        cell = [UserTableViewCell new];
    }
    cell.user = self.users[indexPath.row];
    return cell;
}

@end
