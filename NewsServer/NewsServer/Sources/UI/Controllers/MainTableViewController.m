//
//  MainTableViewController.m
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "MainTableViewController.h"
#import "CCAlertView.h"

@implementation MainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {    
    if  (indexPath.row == 4) {
        CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:@"Предупреждение" message:@"Вы действительно хотите выйти?"];
        [alertView addButtonWithTitle:@"Выйти" block:^{
            [self logout];
        }];
        [alertView addButtonWithTitle:@"Отмена" block:NULL];
        [alertView show];
    }
    
    if (indexPath.row == 5) {
        CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:@"Предупреждение" message:@"Вы действительно хотите удалить свой аккаунт? Это действие необратимо."];
        [alertView addButtonWithTitle:@"Удалить" block:^{
            [self deleteUser];
        }];
        [alertView addButtonWithTitle:@"Отмена" block:NULL];
        [alertView show];
    }
}

- (void)deleteUser {
    [self.sessionManager deleteUser:[self.passwordStorage login] withPassword:[self.passwordStorage password] responce:^(NSError *error) {
        if (!error) {
            [self deleteLoginAndPassword];
        }
    }];
}

- (void)logout {
    [self.sessionManager logoutwithResponce:^(NSError *error) {
        if (!error) {
            [self deleteLoginAndPassword];
        }
    }];
}

- (void)deleteLoginAndPassword {
    [self.passwordStorage deleteLoginAndPassword];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
