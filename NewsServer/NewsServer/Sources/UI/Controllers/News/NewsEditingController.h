//
//  NewsEditingController.h
//  NewsServer
//
//  Created by Anna on 09/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "News.h"

#import "NewsfeedProviderProtocol.h"
#import "NewsProviderProtocol.h"
#import "AccessChecker.h"

@interface NewsEditingController : UIViewController

@property (nonatomic)InjectedProtocol(NewsfeedProviderProtocol)newsfeedProvider;
@property (nonatomic)InjectedProtocol(NewsProviderProtocol)newsProvider;

@property (nonatomic)InjectedClass(AccessChecker)accessChecker;

@property (nonatomic) News *news;

@end
