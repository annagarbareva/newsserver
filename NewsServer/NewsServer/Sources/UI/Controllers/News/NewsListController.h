//
//  NewsListController.h
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProviderProtocol.h"

#import "AccessChecker.h"
#import "OrderProvider.h"

@interface NewsListController : UIViewController

@property (nonatomic) InjectedProtocol(NewsfeedProviderProtocol) newsfeedProvider;
@property (nonatomic) InjectedClass(AccessChecker) accessChecker;
@property (nonatomic) InjectedClass(OrderProvider) orderProvider;
@property (nonatomic) NewsfeedInfo *newsfeedInfo;

@end
