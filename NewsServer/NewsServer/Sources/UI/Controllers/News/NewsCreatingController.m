//
//  NewsCreatingController.m
//  NewsServer
//
//  Created by Anna on 26/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsCreatingController.h"
#import "NSString+Tags.h"

@interface NewsCreatingController ()

@property (weak, nonatomic) IBOutlet UITextField *subject;
@property (weak, nonatomic) IBOutlet UITextView *body;
@property (weak, nonatomic) IBOutlet UITextField *tags;
@property (weak, nonatomic) IBOutlet UISwitch *isPrivate;
@property (weak, nonatomic) IBOutlet UITextField *ttl;
@property (weak, nonatomic) IBOutlet UISlider *priority;

@end

@implementation NewsCreatingController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.newsfeedInfo.name;
}

- (IBAction)onAdd:(id)sender {
    if ([self isCorrectFields]) {
        News *news = [self newsFromFields];
        [self.newsProvider createNews:news responce:^(NSString *newsID, NewsStatus status, NSError *error) {
            if (!error) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
            }
        }];
    }
    else {
        [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:@"Нужно заполнить поля 'тема' и 'тело' новости" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil] show];
    }
    
}

- (News *)newsFromFields {
    News *news = [News new];
    news.newsfeedName = self.newsfeedInfo.name;
    news.subject = self.subject.text;
    news.body = self.body.text;
    news.tags = [self.tags.text tagsArray];
    news.isPrivate = self.isPrivate.on;
    news.ttl = [self.ttl.text intValue] == 0 ? nil : @([self.ttl.text intValue]);
    news.priority = (NewsPriority)self.priority.value;
    
    return news;
}

- (BOOL)isCorrectFields {
    return [self.subject.text length] * [self.body.text length] > 0;
}

- (IBAction)onTouchUpInsidePriority:(id)sender {
    long sliderValue;
    sliderValue = lroundf(self.priority.value);
    [self.priority setValue:sliderValue animated:YES];
}



@end
