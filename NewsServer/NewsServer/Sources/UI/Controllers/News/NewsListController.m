//
//  NewsListController.m
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsListController.h"

#import "News.h"

#import "NewsTableViewCell.h"
#import "NewsController.h"
#import "NewsCreatingController.h"

#import "NSString+Tags.h"
#import "UIViewController+RefreshContolLabel.h"

@interface NewsListController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topTableViewConstraint;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (nonatomic) UIRefreshControl *refreshControl;

@property (nonatomic) Newsfeed *newsfeed;
@property (nonatomic) NSArray *usersNewsList;

@property (nonatomic) NSArray *tags;

@end

@implementation NewsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavigationItemSettings];
    [self setTableViewSettings];
    
    [self receiveNewsList];
    
    [self makeRefreshControlSettings];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveNewsList) name:NewsCreatedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveNewsList) name:NewsMovedNotification object:nil];
}

- (void)setAddButtonActiveIfNeeded:(UIBarButtonItem *)add {
    [self.accessChecker hasUserFullStatusWithResponse:^(BOOL hasFullStatus, NSError *error) {
        [add setEnabled:hasFullStatus];
    }];
}

#pragma mark - Navigation item settings

- (void)setNavigationItemSettings {
    self.navigationItem.title = self.newsfeedInfo.name;
    [self setBarButtonItems];
}

- (void)setBarButtonItems {
    UIBarButtonItem *search = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(onSearch)];
    UIBarButtonItem *add = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(onAdd)];
    [self setAddButtonActiveIfNeeded:add];
    NSArray *buttons = @[add, search];
    self.navigationItem.rightBarButtonItems = buttons;
}

#pragma  mark - Bar buttton actions

- (void)onSearch {
    self.topTableViewConstraint.constant = 40;
    
}

- (void)onAdd {
    NewsCreatingController *destinationVC = (NewsCreatingController *)[self.storyboard instantiateViewControllerWithIdentifier:@"NewsCreatingController"];
    destinationVC.newsfeedInfo = self.newsfeedInfo;
    [self.navigationController showViewController:destinationVC sender:self];
}

#pragma  mark - Table View settings

- (void)setTableViewSettings {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 44.0;
    
    [self registerNib];
}

- (void)registerNib {
    UINib *nib = [UINib nibWithNibName:@"NewsTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:[NewsTableViewCell identifier]];
}

#pragma mark - Receiving NewsList

- (void)receiveNewsList {
    if (self.refreshControl){
        self.refreshControl.attributedTitle = [self attributedLabelToRefreshControl];
    }
    [self.newsfeedProvider requestNewsfeedNewsList:self.newsfeedInfo tags: self.tags response:^(Newsfeed *newsfeed, NSError *error) {
        if (!error) {
            self.newsfeed = newsfeed;
            self.usersNewsList = [self.orderProvider orderedNewsListFromArray: newsfeed.usersNewsList];
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        }
    }];
}

#pragma  mark - Refresh control

- (void)makeRefreshControlSettings {
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(receiveNewsList) forControlEvents:UIControlEventValueChanged];
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsController *destinationController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsController"];
    destinationController.news = [self newsFromIndexPathRow:indexPath.row];
    [self.navigationController pushViewController:destinationController animated:YES];
}

- (News *)newsFromIndexPathRow:(NSInteger)index {
    NSInteger importantNewsCount = [self.newsfeed.importantNewsList count];
    NSInteger userNewsCount = [self.usersNewsList count];
    if (index < importantNewsCount) {
        return self.newsfeed.importantNewsList[index];
    }
    else if (index < importantNewsCount + userNewsCount) {
        return self.usersNewsList[index - importantNewsCount];
    }
    return nil;
}

#pragma mark - TableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsTableViewCell identifier]];
    if (!cell) {
        cell = [NewsTableViewCell new];
    }
    NSArray *importantNewsList = self.newsfeed.importantNewsList;
    NSInteger importantNewsCount = [importantNewsList count];
    
    if (indexPath.row < importantNewsCount) {
        cell.news = importantNewsList[indexPath.row];
    }
    else {
        cell.news = self.usersNewsList[indexPath.row - importantNewsCount];
    }
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.newsfeed.importantNewsList count] + [self.usersNewsList count];
}

#pragma mark -

- (IBAction)onDidFinishWriteTags:(id)sender {
    self.tags = [self.searchField.text tagsArray];
    if ([self.tags count] > 0) {
        self.usersNewsList = [self.newsfeed usersNewsListWithTags:self.tags];
    }
    else {
        self.usersNewsList = self.newsfeed.usersNewsList;
    }
    [self.tableView reloadData];
}

#pragma mark -

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.topTableViewConstraint.constant = 0;
}

#pragma mark - Dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NewsCreatedNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NewsMovedNotification object:nil];
}

@end
