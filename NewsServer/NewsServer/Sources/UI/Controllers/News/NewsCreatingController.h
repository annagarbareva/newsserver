//
//  NewsCreatingController.h
//  NewsServer
//
//  Created by Anna on 26/05/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsProviderProtocol.h"
#import "NewsfeedInfo.h"

#import "AccessChecker.h"

@interface NewsCreatingController : UIViewController

@property (nonatomic) InjectedProtocol(NewsProviderProtocol) newsProvider;
@property (nonatomic) InjectedClass(AccessChecker) accessChecker;
@property (nonatomic) NewsfeedInfo *newsfeedInfo;

@end
