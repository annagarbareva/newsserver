//
//  NewsEditingController.m
//  NewsServer
//
//  Created by Anna on 09/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsEditingController.h"
#import "ImportantNews.h"

@interface NewsEditingController () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIPickerView *newsfeedsPicker;
@property (weak, nonatomic) IBOutlet UITextField *subject;
@property (weak, nonatomic) IBOutlet UITextView *body;
@property (weak, nonatomic) IBOutlet UISlider *priority;

@property (nonatomic) NSArray *newsfeedsNames;

@end

@implementation NewsEditingController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self makePickerViewSettings];
    [self makePlaceholders];
    [self setEditingFieldsActiveIfNeeded];
    [self setNewsfeedPickerActiveIfNeeded];
}

- (void)setEditingFieldsActiveIfNeeded {
    [self.accessChecker hasUserFullStatusWithResponse:^(BOOL hasFullStatus, NSError *error) {
        self.body.editable = hasFullStatus;
        [self.subject setUserInteractionEnabled:hasFullStatus];
    }];
}

- (void)setNewsfeedPickerActiveIfNeeded {
    if (![self.accessChecker canChangeNewsfeedNameInNews:self.news]) {
        [self.newsfeedsPicker setUserInteractionEnabled:NO];
    }
}

- (void)setAccessConstraintsForNewsfeedName {
    if (self.news.newsStatus == NewsStatusModeration) {
        [self.newsfeedsPicker setUserInteractionEnabled:NO];
    }
}

#pragma mark - PickerView

- (void)makePickerViewSettings {
    self.newsfeedsPicker.delegate = self;
    self.newsfeedsPicker.dataSource = self;
    [self receiveNewsfeedList];
}

- (void)receiveNewsfeedList {
    [self.newsfeedProvider requestNewsfeedList:^(NSArray *newsfeeds, NSError *error) {
        if (!error) {
            [self fillNewsfeedsNamesFromNewsfeedsInfoArray:newsfeeds];
            [self.newsfeedsPicker reloadAllComponents];
            [self setCurrentNewsfeedName];
        }
    }];
}

- (void)fillNewsfeedsNamesFromNewsfeedsInfoArray:(NSArray *)newsfeeds {
    NSMutableArray *result = [[NSMutableArray alloc]initWithCapacity:[newsfeeds count]];
    for (NewsfeedInfo *info in newsfeeds) {
        [result addObject:info.name];
    }
    self.newsfeedsNames = result;
}

- (void)setCurrentNewsfeedName
{
    NSString *currentNewsfeedName = self.news.newsfeedName;
    NSInteger index = [self.newsfeedsNames indexOfObject:currentNewsfeedName];
    [self.newsfeedsPicker selectRow:index inComponent:0 animated:NO];
}

#pragma  mark -

- (void)makePlaceholders {
    self.subject.text = self.news.subject;
    self.body.text = self.news.body;
    [self.priority setValue:self.news.priority];
}

#pragma mark - Priority slider

- (IBAction)onTouchUpInsidePriority:(id)sender {
    long sliderValue;
    sliderValue = lroundf(self.priority.value);
    [self.priority setValue:sliderValue animated:YES];
}

#pragma mark - IBActions

- (IBAction)onSave:(id)sender {
    [self editNewsBodyIfNeeded];
    [self editNewsPriorityIfNeeded];
    [self editNewsNewsfeedIfNeeded];
}

- (void)editNewsBodyIfNeeded {
    if (![self.body.text isEqualToString:self.news.body] || ![self.subject.text isEqualToString:self.news.subject]) {
        [self.newsProvider editNews:self.news withSubject:self.subject.text withBody:self.body.text responce:^(NSError *error) {
            [self handleResponseWithError:error];
        }];
    }
}

- (void)editNewsPriorityIfNeeded {
    if ((NewsPriority)self.priority.value != self.news.priority) {
        [self.newsProvider editNews:self.news withPriority:(NewsPriority)self.priority.value responce:^(NSError *error) {
            [self handleResponseWithError:error];
        }];
    }
}

- (void)editNewsNewsfeedIfNeeded {
    NSInteger currentIndex = [self.newsfeedsPicker selectedRowInComponent:0];
    if (![self.newsfeedsNames[currentIndex] isEqualToString:self.news.newsfeedName]) {
        [self.newsProvider editNews:self.news withNewsfeedName:self.newsfeedsNames[currentIndex] responce:^(NSError *error) {
            [self handleResponseWithError:error];
        }];
    }
}

- (IBAction)onDelete:(id)sender {
    [self.newsProvider deleteNews:self.news responce:^(NSError *error) {
        [self handleResponseWithError:error];
    }];
}

- (void)handleResponseWithError:(NSError *)error {
    if (!error) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else {
        [[[UIAlertView alloc]initWithTitle:@"Ошибка" message:error.localizedDescription delegate:self cancelButtonTitle:@"ОК" otherButtonTitles: nil]show];
    }
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.newsfeedsNames count];
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.newsfeedsNames[row];
}


@end
