//
//  NewsTypesController.m
//  NewsServer
//
//  Created by Anna on 04/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsTypesController.h"

#import "SimpleTableViewCell.h"
#import "UserNewsListController.h"

#import "News.h"

@interface NewsTypesController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation NewsTypesController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTableViewSettings];
}

#pragma mark - Table View settings

- (void) setTableViewSettings {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self registerNib];
}

- (void)registerNib {
    UINib *nib = [UINib nibWithNibName:@"SimpleTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:[SimpleTableViewCell identifier]];
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UserNewsListController *destinationController = (UserNewsListController *)[self.storyboard instantiateViewControllerWithIdentifier:@"UserNewsListController"];
    destinationController.status = (NewsStatus)indexPath.row;
    [self.navigationController pushViewController:destinationController animated:YES];
}

#pragma mark - TableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[SimpleTableViewCell identifier]];
    if (!cell) {
        cell = [SimpleTableViewCell new];
    }
    [cell setTitle: [News newsStatusStringFromStatus:(NewsStatus)indexPath.row]];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [News numberOfStatuses];
}

@end
