//
//  NewsController.m
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsController.h"

#import "NewsEditingController.h"

@interface NewsController ()

@property (weak, nonatomic) IBOutlet UILabel *newsfeedName;
@property (weak, nonatomic) IBOutlet UITextView *newsBody;
@property (weak, nonatomic) IBOutlet UILabel *priority;
@property (weak, nonatomic) IBOutlet UILabel *tags;
@property (weak, nonatomic) IBOutlet UILabel *creationDate;
@property (weak, nonatomic) IBOutlet UILabel *deathDate;
@property (weak, nonatomic) IBOutlet UILabel *status;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *editButton;

@end

@implementation NewsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self fillTextIntoControls];
    [self setEditButtonActiveIfNeeded];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNews) name:NewsEditedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(popViewController) name:NewsMovedNotification object:nil];
}

- (void)fillTextIntoControls {
    self.navigationItem.title = self.news.subject;
    self.newsfeedName.text = self.news.newsfeedName;
    self.newsBody.text = self.news.body;
    self.priority.text = [NSString stringWithFormat:@"Приоритет %@", [self.news priorityString]];
    self.tags.text = [self.news tagsString];
    self.creationDate.text = [NSString stringWithFormat:@"Дата создания: %@", [self.news createdDateString]];
    self.deathDate.text = [NSString stringWithFormat:@"Дата удаления: %@", [self.news deathDateString]];
    self.status.text = [NSString stringWithFormat:@"Cтатус: '%@'", [self.news newsStatusString]];
}

- (void) setEditButtonActiveIfNeeded {
    if (![self.accessChecker canEditNews:self.news]) {
        [self.editButton setEnabled:NO];
    }
}

#pragma mark - Notification actions

- (void)updateNews {
    [self.newsProvider requestNewsWithNewsID:self.news.newsID newsfeedName:self.news.newsfeedName responce:^(News *news, NSError *error) {
        if (!error) {
            self.news = news;
            [self fillTextIntoControls];
        }
    }];
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark -

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"NewsEditingSegue"]) {
        NewsEditingController *destinationController = segue.destinationViewController;
        destinationController.news = self.news;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NewsEditedNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:NewsMovedNotification object:nil];
}

@end
