//
//  NewsController.h
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsProviderProtocol.h"

#import "AccessChecker.h"

@interface NewsController : UIViewController

@property (nonatomic) News *news;
@property (nonatomic) InjectedClass(AccessChecker) accessChecker;
@property (nonatomic) InjectedProtocol(NewsProviderProtocol) newsProvider;

@end
