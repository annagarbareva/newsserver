//
//  UserNewsListController.h
//  NewsServer
//
//  Created by Anna on 04/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserProviderProtocol.h"
#import "News.h"

@interface UserNewsListController : UITableViewController

@property (nonatomic) InjectedProtocol(UserProviderProtocol) userProvider;

@property (nonatomic) NewsStatus status;

@end
