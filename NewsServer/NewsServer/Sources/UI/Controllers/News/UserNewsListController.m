//
//  UserNewsListController.m
//  NewsServer
//
//  Created by Anna on 04/06/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserNewsListController.h"

#import "NewsTableViewCell.h"
#import "NewsController.h"

@interface UserNewsListController ()

@property (nonatomic) NSArray *newsList;

@end

@implementation UserNewsListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNib];
    [self receiveNews];
    self.navigationItem.title = [News newsStatusStringFromStatus:self.status];
}

- (void)registerNib {
    UINib *nib = [UINib nibWithNibName:@"NewsTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:[NewsTableViewCell identifier]];
}

#pragma mark - Receiving news list

- (void)receiveNews {
    [self.userProvider requestUserNewsList:^(UserNewsList *newsList, NSError *error) {
        if (!error) {
            [self chooseNewsListFromUserNewsList:newsList];
            [self.tableView reloadData];
        }
    }];
}

- (void)chooseNewsListFromUserNewsList:(UserNewsList *)newsList {
    switch (self.status) {
        case NewsStatusAccepted:
            self.newsList = newsList.acceptedNewsList;
            break;
        case NewsStatusModeration:
            self.newsList = newsList.moderatedNewsList;
            break;
        case NewsStatusRejected:
            self.newsList = newsList.rejectedNewsList;
            break;
        default:
            break;
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.newsList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NewsTableViewCell identifier] forIndexPath:indexPath];
    if (!cell) {
        cell = [NewsTableViewCell new];
    }
    cell.news = self.newsList[indexPath.row];
    return cell;
}

#pragma mark = Table view delegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsController *destinationController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsController"];
    destinationController.news = self.newsList[indexPath.row];
    [self.navigationController pushViewController:destinationController animated:YES];
}

@end
