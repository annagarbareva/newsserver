//
//  NewsfeedCreatingController.m
//  NewsServer
//
//  Created by Sovelu on 03.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedCreatingController.h"

#import "UIViewController+ErrorHandling.h"

@interface NewsfeedCreatingController()

@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UISwitch *moderation;

@end

@implementation NewsfeedCreatingController

- (IBAction)onCreate:(id)sender
{
    if ([self.validator isCorrectNewsfeedName:self.name.text]) {
        [self.newsFeedProvider createNewsfeed:self.name.text type:[self newsfeedTypeFromSwitch:self.moderation] response:^(NSError *error) {
            if (!error) {
                [self.navigationController popViewControllerAnimated:YES];
            }
            [self handleError:error];
        }];
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Имя новостной ленты должно содержать латинские или русские буквы, а его длина должна быть от 1 до 50 символов" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    };
}

- (NewsfeedType)newsfeedTypeFromSwitch: (UISwitch *)switchPosition {
    if ([switchPosition isOn]) {
        return NewsfeedTypeModerated;
    }
    return NewsfeedTypeNonModerated;
}

@end
