//
//  NewsfeedController.h
//  NewsServer
//
//  Created by Sovelu on 03.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProviderProtocol.h"
#import "NewsfeedInfo.h"

#import "AccessChecker.h"

@interface NewsfeedController : UIViewController

@property (nonatomic, weak) NewsfeedInfo *newsfeedInfo;

@property (nonatomic) InjectedProtocol(NewsfeedProviderProtocol) newsfeedProvider;
@property (nonatomic) InjectedClass(AccessChecker) accessChecker;

@end
