//
//  NewsfeedListController.m
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedListController.h"

#import "SimpleTableViewCell.h"
#import "UIViewController+RefreshContolLabel.h"

#import "NewsfeedController.h"
#import "NewsfeedCreatingController.h"

@interface NewsfeedListController ()

@property (nonatomic, strong) NSArray *newsfeedInfoList;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addButton;

@end

@implementation NewsfeedListController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerClassAndNib];
    [self makeRefreshControlSettings];
    [self receiveNewsfeedList];
    [self setAddButtonEnabledIfNeeded];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveNewsfeedList) name:NewsfeedCreatedNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(receiveNewsfeedList) name:NewsfeedDeletedNotification object:nil];
}

- (void)registerClassAndNib {
    UINib *nib = [UINib nibWithNibName:@"SimpleTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:[SimpleTableViewCell identifier]];
}

- (void)makeRefreshControlSettings {
    self.refreshControl = [UIRefreshControl new];
    [self.refreshControl addTarget:self action:@selector(receiveNewsfeedList) forControlEvents:UIControlEventValueChanged];
}

- (void)receiveNewsfeedList {
    if (self.refreshControl){
        self.refreshControl.attributedTitle = [self attributedLabelToRefreshControl];
    }
    [self.newsfeedProvider requestNewsfeedList:^(NSArray *newsfeeds, NSError *error) {
        if (!error) {
            self.newsfeedInfoList = newsfeeds;
            [self.refreshControl endRefreshing];
            [self.tableView reloadData];
        }
    }];
}

- (void)setAddButtonEnabledIfNeeded {
    [self.accessChecker hasUserFullStatusWithResponse:^(BOOL hasAccess, NSError *error) {
            [self.addButton setEnabled:hasAccess];
    }];
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NewsfeedController *destinationController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsfeedController"];
    destinationController.newsfeedInfo = [self.newsfeedInfoList objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:destinationController animated:YES];
}

#pragma mark - TableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[SimpleTableViewCell identifier]];
    if (!cell) {
        cell = [SimpleTableViewCell new];
    }
    NewsfeedInfo *newsfeeedInfo = [self.newsfeedInfoList objectAtIndex:indexPath.row];
    NSString *title = newsfeeedInfo.name;
    [cell setTitle:title];
    return cell;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger numberOfRows = [self.newsfeedInfoList count];
    if (numberOfRows == 0) {
        [self addMessageLabel];
    }
    else {
        self.tableView.backgroundView = nil;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    }
    return numberOfRows;
}

- (void)addMessageLabel {
    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    
    messageLabel.text = @"Пока еще нет ни одной новостной ленты.";
    messageLabel.numberOfLines = 0;
    messageLabel.textAlignment = NSTextAlignmentCenter;
    messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
    [messageLabel sizeToFit];
    
    self.tableView.backgroundView = messageLabel;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark - dealloc

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NewsfeedCreatedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NewsfeedDeletedNotification object:nil];
}

@end
