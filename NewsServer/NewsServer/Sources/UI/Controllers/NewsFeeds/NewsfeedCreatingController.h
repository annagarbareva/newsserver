//
//  NewsfeedCreatingController.h
//  NewsServer
//
//  Created by Sovelu on 03.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProviderProtocol.h"
#import "Newsfeed.h"

#import "Validator.h"

@interface NewsfeedCreatingController : UIViewController

@property (nonatomic, strong) InjectedProtocol(NewsfeedProviderProtocol) newsFeedProvider;
@property (nonatomic) InjectedClass(Validator) validator;

@end
