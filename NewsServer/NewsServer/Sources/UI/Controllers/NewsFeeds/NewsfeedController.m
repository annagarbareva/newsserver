//
//  NewsfeedController.m
//  NewsServer
//
//  Created by Sovelu on 03.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedController.h"
#import "NewsListController.h"

#import "CCAlertView.h"

@interface NewsfeedController()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorName;
@property (weak, nonatomic) IBOutlet UIImageView *moderatedSign;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *deleteButton;

@end

@implementation NewsfeedController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self receiveNewsfeedInfo];
    [self setDeleteButtonActiveIfNeeded];
    self.navigationItem.title = self.newsfeedInfo.name;
}

- (void)setDeleteButtonActiveIfNeeded {
    if (![self.accessChecker canDeleteNewsfeed:self.newsfeedInfo]) {
        [self.deleteButton setEnabled:NO];
    }
}

- (void)popViewController {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)receiveNewsfeedInfo
{
    [self.newsfeedProvider requestNewsfeedInfo:self.newsfeedInfo.name response:^(NewsfeedInfo *info, NSError *error) {
        self.nameLabel.text = info.name;
        self.authorName.text = [NSString stringWithFormat:@"Author: %@",info.authorName];
        self.moderatedSign.image = [UIImage imageNamed:[self moderatedImageNameFromNewsfeedType:info.type]];
    }];
}

- (NSString *)moderatedImageNameFromNewsfeedType: (NewsfeedType) type {
    switch (type) {
        case NewsfeedTypeModerated:
            return @"Moderated";
            break;
        case NewsfeedTypeNonModerated:
            return @"NonModerated";
        default:
            return @"";
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"NewsListSegue"]) {
        NewsListController *newsListVC = [segue destinationViewController];
        newsListVC.newsfeedInfo = self.newsfeedInfo;
    }
}

- (IBAction)onDelete:(id)sender {
    CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:@"" message:@"Вы действительно хотите удалить ЛН?"];
    [alertView addButtonWithTitle:@"Удалить" block:^{
        [self.newsfeedProvider deleteNewsfeed:self.newsfeedInfo.name response:^(NSError *error) {
            if (!error) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }];
    [alertView addButtonWithTitle:@"Отмена" block:NULL];
    [alertView show];
}

@end
