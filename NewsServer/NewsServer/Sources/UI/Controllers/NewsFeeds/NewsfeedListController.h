//
//  NewsfeedListController.h
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsfeedProviderProtocol.h"
#import "AccessChecker.h"

@interface NewsfeedListController : UITableViewController

@property (nonatomic) InjectedProtocol(NewsfeedProviderProtocol) newsfeedProvider;
@property (nonatomic) InjectedClass(AccessChecker) accessChecker;

@end
