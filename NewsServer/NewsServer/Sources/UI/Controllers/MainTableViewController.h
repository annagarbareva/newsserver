//
//  MainTableViewController.h
//  NewsServer
//
//  Created by Anna on 27/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "SessionManagerProtocol.h"
#import "LoginAndPasswordStorage.h"

#import "DebugProviderProtocol.h"

@interface MainTableViewController : UITableViewController

@property (nonatomic) InjectedProtocol(SessionManagerProtocol) sessionManager;
@property (nonatomic) InjectedClass(LoginAndPasswordStorage) passwordStorage;

@property (nonatomic) InjectedProtocol(DebugProviderProtocol) debugProvider;

@end

