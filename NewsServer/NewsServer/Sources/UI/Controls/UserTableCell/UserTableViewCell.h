//
//  UserTableViewCell.h
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "User.h"

@interface UserTableViewCell : UITableViewCell

@property (nonatomic) User *user;

+ (NSString *)identifier;

@end
