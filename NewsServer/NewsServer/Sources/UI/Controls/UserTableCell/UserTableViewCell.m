//
//  UserTableViewCell.m
//  NewsServer
//
//  Created by Sovelu on 07.06.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "UserTableViewCell.h"
#import "NSDate+stringDate.h"

@interface UserTableViewCell()

@property (weak, nonatomic) IBOutlet UIImageView *online;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *registrationDate;
@property (weak, nonatomic) IBOutlet UILabel *status;

@end

@implementation UserTableViewCell

+ (NSString *)identifier {
    return @"UserTableViewCell";
}

- (void)setUser:(User *)user {
    _user = user;
    [self fillOnlineLabel];
    self.name.text = self.user.name;
    self.registrationDate.text = [NSString stringWithFormat:@"%@", [self.user.timeRegistered stringDate]];
    [self fillStatusLabel];
}

- (void)fillOnlineLabel {
    NSString *imageName = @"";
    if (self.user.isActive) {
        imageName = @"online";
    }
    else {
        imageName = @"offline";
    }
    self.online.image = [UIImage imageNamed:imageName];
}

- (void)fillStatusLabel {
    switch (self.user.status) {
        case UserStatusFull:
            self.status.text = @"Статус полный";
        break;
        case UserStatusLimited:
            self.status.text = [NSString stringWithFormat:@"Статус ограниченный, до %@", [self.user.banExitDate stringDate]];
        break;
        default:
        break;
    }
}

@end
