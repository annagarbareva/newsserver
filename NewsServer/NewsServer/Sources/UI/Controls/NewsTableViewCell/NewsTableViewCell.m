//
//  NewsTableViewCell.m
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "NewsTableViewCell.h"
#import "ImportantNews.h"

@interface NewsTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *newsfeedName;
@property (weak, nonatomic) IBOutlet UILabel *votes;
@property (weak, nonatomic) IBOutlet UILabel *subject;
@property (weak, nonatomic) IBOutlet UILabel *tags;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *subjectTagsConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *newsfeedNameVotesConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *votesSubjectConstraint;

@end

@implementation NewsTableViewCell

+ (NSString *)identifier
{
    return @"NewsTableViewCell";
}

- (void)setNews:(News *)news {
    _news = news;
    self.backgroundColor = [UIColor clearColor];
    self.subject.text = self.news.subject;
    [self fillTagsLabel];
    [self fillNewsfeedNameLabel];
    [self fillVotesLabel];
    [self makePrivateMarkIfNeeded];
}

- (void)fillTagsLabel {
    if ([self.news.tags count] > 0) {
        NSString *tagsString = @"теги: ";
        for (NSString *tag in self.news.tags) {
            tagsString = [NSString stringWithFormat:@"%@%@, ",tagsString,tag];
        }
        tagsString = [tagsString substringToIndex:[tagsString length] - 2];
        self.tags.text = tagsString;
    }
    else {
        self.tags.text = @"";
        self.subjectTagsConstraint.constant = 0;
    }
}

- (void)fillNewsfeedNameLabel {
    if (self.news.newsfeedName) {
        self.newsfeedName.text = self.news.newsfeedName;
    }
    else {
        self.newsfeedName.text = @"";
        self.newsfeedNameVotesConstraint.constant = 0;
    }
}

- (void)fillVotesLabel {
    if (self.news.votesContra > 0) {
        self.votes.text = [NSString stringWithFormat:@"за: %lu, против: %lu", (unsigned long)self.news.votesPro, (unsigned long)self.news.votesContra];
    }
    else {
        self.votes.text = @"";
        self.votesSubjectConstraint.constant = 0;
    }
}

- (void)makePrivateMarkIfNeeded {
    if (self.news.isPrivate) {
        self.backgroundColor = [UIColor grayColor];
    }
}

- (void)makeImportantMarkIfNeeded {
    if ([self.news isKindOfClass:[ImportantNews class]]) {
        self.backgroundColor = [UIColor yellowColor];
    }
}

@end
