//
//  NewsTableViewCell.h
//  NewsServer
//
//  Created by Sovelu on 25.05.15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "News.h"

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic) News *news;

+ (NSString *)identifier;

@end
