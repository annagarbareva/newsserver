//
//  SimpleTableViewCell.h
//  NewsServer
//
//  Created by Anna on 29/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableViewCell : UITableViewCell

- (void)setTitle:(NSString *)title;
+ (NSString *)identifier;

@end
