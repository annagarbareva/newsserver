//
//  SimpleTableViewCell.m
//  NewsServer
//
//  Created by Anna on 29/04/15.
//  Copyright (c) 2015 Anna. All rights reserved.
//

#import "SimpleTableViewCell.h"

@interface SimpleTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@end

@implementation SimpleTableViewCell

- (void)setTitle:(NSString *)title
{
    [self.titleLabel setText:title];
}

+ (NSString *)identifier
{
    return @"SimpleTableViewCell";
}

@end
